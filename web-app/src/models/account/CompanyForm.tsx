import React, { useState } from 'react';
import { Box, Flex, Collapse, Stack, useToast, Grid, Text } from '@chakra-ui/react';
import { Formik, Form } from 'formik';
import { FaArrowUp, FaArrowDown } from 'react-icons/fa';

import {
  CompanyInputModel,
  CreateCompanyMutationResult,
  useCreateCompanyMutation,
  useDeleteCompanyMutation,
  useUpdateCompanyMutation,
} from '../../generated/graphql';
import { InputField, Button, DropdownField } from '../../common/components/shared/atoms';
import { ApolloError } from '@apollo/client';
import { countryOptions } from '../../common/types/countries';
import { setValidations, getBorderColor } from './utils';
import { useIntl } from 'react-intl';
import { defaultErrorToastConfig, defaultSuccessToastConfig, defaultToastConfig } from '../../utils/constants';

interface ICompanyFormProps {
  data: CompanyInputModel;
  creatorId?: number;
  onDelete: (id: number) => void;
  setExpandedId: React.Dispatch<React.SetStateAction<number | null>>;
  isExpanded: boolean;
}

export const CompanyForm: React.FC<ICompanyFormProps> = ({ data, onDelete, setExpandedId, isExpanded }) => {
  const intl = useIntl();
  const toast = useToast();
  const [isFormValid, setIsFormValid] = useState(true);

  const [updateMutation, { error: updateError }] = useUpdateCompanyMutation();
  const [createMutation, { error: createError }] = useCreateCompanyMutation();
  const [deleteMutation] = useDeleteCompanyMutation();

  const onSubmitPostCallback = async (companyData: CompanyInputModel, setErrors: (error: ApolloError) => void) => {
    let response;
    if (companyData.id) {
      response = await updateMutation({
        variables: { id: companyData.id, input: { ...companyData } as CompanyInputModel },
      });
    } else {
      response = await createMutation({
        variables: { input: { ...companyData } as CompanyInputModel },
      });
      if (response.data) {
        data.id = (response as CreateCompanyMutationResult).data?.createCompany.id;
        toast({
          ...defaultToastConfig,
          title: 'Success',
          description: intl.formatMessage({
            id: 'Address.CompanyForm.CreatedToast',
            defaultMessage: 'Company created',
          }),
          status: 'success',
        });
      }
    }
    if (response) {
      if (updateError) {
        setErrors(updateError);
      }
      if (createError) {
        setErrors(createError);
      }
    }
  };

  const onDeletePostCallback = async (id: number) => {
    try {
      await deleteMutation({
        variables: { id },
        update: (cache) => {
          cache.evict({ id: 'Company:' + id });
        },
      });
      onDelete(id);
      toast({
        ...defaultSuccessToastConfig(intl),
        description: intl.formatMessage({
          id: 'Account.Form.Toast.SuccessBody',
          defaultMessage: 'Company was successfully deleted',
        }),
      });
    } catch {
      toast(defaultErrorToastConfig(intl));
    }
  };

  return (
    <Formik
      initialValues={data}
      onSubmit={(values, { setErrors }) => onSubmitPostCallback(values, setErrors)}
      validate={(data) => setValidations(data, setIsFormValid)}>
      {({ isSubmitting, submitForm, values }) => (
        <Form>
          <Flex
            position="relative"
            marginTop="8"
            marginBottom="4"
            borderBottom="1px"
            borderColor={getBorderColor(isFormValid, !data.id)}
            onClick={() => setExpandedId((prev) => (data.id !== undefined && prev !== data.id ? data.id : null))}
            _hover={{ cursor: 'pointer' }}>
            <Box mr="auto">
              <Text fontWeight="700" fontSize={'2xl'}>
                {values.name}
              </Text>
            </Box>
            <Flex direction="row" ml="auto" my={4}>
              <Box px="4">{isExpanded ? <FaArrowUp /> : <FaArrowDown />}</Box>
            </Flex>
          </Flex>

          <Collapse in={isExpanded}>
            <Grid gridTemplateColumns={{ lg: 'repeat(3, 1fr)', md: 'repeat(2, 1fr)', sm: '1fr' }} gap={'8px'} mb={4}>
              <InputField
                label={intl.formatMessage({ id: 'Account.Form.Name', defaultMessage: 'Name' })}
                name="name"
                required
                disabled={!!data.id}
              />
              <InputField
                label={intl.formatMessage({ id: 'Account.Form.CIN', defaultMessage: 'CIN' })}
                name="cin"
                required
              />
              <InputField label={intl.formatMessage({ id: 'Account.Form.VAT', defaultMessage: 'VAT' })} name="vat" />
            </Grid>

            {/* <AdditionalCompanyForm setFieldValue={setFieldValue} data={values} /> */}
            <Grid gridTemplateColumns={{ lg: 'repeat(3, 1fr)', md: 'repeat(2, 1fr)', sm: '1fr' }} gap={'8px'}>
              <DropdownField
                label={intl.formatMessage({ id: 'Account.Form.Country', defaultMessage: 'Country' })}
                name="countryCode"
                items={countryOptions}
              />
              <InputField
                label={intl.formatMessage({ id: 'Account.Form.City', defaultMessage: 'City' })}
                name="city"
                required
              />
              <InputField
                label={intl.formatMessage({ id: 'Account.Form.Street', defaultMessage: 'Street' })}
                name="street"
                required
              />
              <InputField
                label={intl.formatMessage({ id: 'Account.Form.StreetNumber', defaultMessage: 'StreetNumber' })}
                name="streetNumber"
                required
              />
              <InputField
                label={intl.formatMessage({ id: 'Account.Form.District', defaultMessage: 'District' })}
                name="district"
                required
              />
              <InputField
                label={intl.formatMessage({ id: 'Account.Form.Website', defaultMessage: 'Website' })}
                name="website"
              />
              <InputField
                label={intl.formatMessage({ id: 'Account.Form.PostCode', defaultMessage: 'PostCode' })}
                name="postalCode"
                required
              />
            </Grid>

            <Flex>
              <Stack direction="row" spacing={4} ml="auto" mt="4">
                <Button
                  colorScheme="red"
                  onClick={() => onDeletePostCallback(Number(data.id))}
                  disabled={!data.id}
                  title="Delete"
                />
                <Button
                  type="submit"
                  onClick={submitForm}
                  isLoading={isSubmitting}
                  title={data.id ? 'Edit' : 'Create'}
                />
              </Stack>
            </Flex>
          </Collapse>
        </Form>
      )}
    </Formik>
  );
};
