import { CompanyInputModel } from '../../generated/graphql';

export const setValidations = (
  data: CompanyInputModel,
  setIsFormValid: (value: React.SetStateAction<boolean>) => void,
) => {
  const errors = {} as any;

  for (const [key, value] of Object.entries(data)) {
    if (!value) {
      if (key === 'name') {
        errors[key] = `Field is required`;
      }
      if (key === 'cin') {
        errors[key] = `Field is required`;
      }
      if (key === 'city') {
        errors[key] = `Field is required`;
      }
      if (key === 'street') {
        errors[key] = `Field is required`;
      }
      if (key === 'streetNumber') {
        errors[key] = `Field is required`;
      }
      if (key === 'district') {
        errors[key] = `Field is required`;
      }
      if (key === 'postalCode') {
        errors[key] = `Field is required`;
      }
    }
  }

  if (Object.entries(errors).length) {
    setIsFormValid(false);
  } else {
    setIsFormValid(true);
  }

  return errors;
};

export const getBorderColor = (isValid: boolean, isNew: boolean) => {
  let borderColor = 'gray.400';
  if (!isValid) {
    borderColor = 'red.400';
  } else if (isNew) {
    borderColor = 'green.400';
  }
  return borderColor;
};
