import { Heading } from '@chakra-ui/react';
import React from 'react';
import { FormattedMessage } from 'react-intl';

import { LayoutComponent } from '../common/components';
import { BodyContainer, Wrapper } from '../common/components/shared/atoms';
import { useIsAuth } from '../common/hooks/useIsAuth';
import { CreateDemandForm } from '../models/posts/CreateDemandForm';
import { withApollo } from '../utils/withApollo';

const CreateDemand: React.FC = () => {
  useIsAuth();

  return (
    <LayoutComponent>
      <BodyContainer type="form">
        <Wrapper variant="regular">
          <Heading as="h2" mb="4">
            <FormattedMessage id="Demand.Create.Title" defaultMessage="Create demand post" />
          </Heading>
          <CreateDemandForm />
        </Wrapper>
      </BodyContainer>
    </LayoutComponent>
  );
};

export default withApollo({ ssr: false })(CreateDemand);
