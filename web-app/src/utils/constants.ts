import { UseToastOptions } from '@chakra-ui/react';
import { IntlShape } from 'react-intl';

export const colorScheme = 'green';
export const mainColor = 'green.400';
export const lightGray = 'gray.400';
export const gray = 'gray.500';

// TODO: hide for prod
export const GOOGLE_API_KEY = 'AIzaSyBmQmNVqOZlkqnUHrV1XDEZNjmvGjZqeAE';

export const defaultToastConfig = {
  duration: 5000,
  isClosable: true,
};

export const defaultSuccessToastConfig = (intl: IntlShape): UseToastOptions => ({
  ...defaultToastConfig,
  title: intl.formatMessage({
    id: 'General.Toast.SuccessTitle',
    defaultMessage: 'Success',
  }),
  status: 'success',
});

export const defaultErrorToastConfig = (intl: IntlShape): UseToastOptions => ({
  ...defaultToastConfig,
  title: intl.formatMessage({
    id: 'General.Toast.ErrorTitle',
    defaultMessage: 'Error',
  }),
  description: intl.formatMessage({
    id: 'General.Toast.ErrorBody',
    defaultMessage: 'An error occured. Please try again later',
  }),
  status: 'error',
});
