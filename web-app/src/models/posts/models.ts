export interface ICreateDemandValues {
  title: string;
  text: string;
  companyId: number;
  categoryId: number;
  countryCode: string;
  locality: string;
  district: string;
  city: string;
  postalCode: string;
  street: string;
  streetNumber: string;
  duration: number;
  useCompanyAddress: boolean;
}

export interface ICreateSupplyValues {
  title: string;
  text: string;
  companyId: number;
  categoryId: number;
  countryCode: string;
  locality: string;
  district: string;
  city: string;
  postalCode: string;
  street: string;
  streetNumber: string;
  duration: number;
  useCompanyAddress: boolean;
}

export interface ICategoriesQuery {
  __typename: string;
  id: number;
  name: string;
  children: ICategoryTreeElement[];
}

export interface ICategoryTreeElement extends ICategoriesQuery {
  isExpanded?: boolean;
  isSelected?: boolean;
  isHighlighted?: boolean;
}

export const initCreateDemandValues: ICreateDemandValues = {
  title: '',
  text: '',
  companyId: 1,
  categoryId: 0,
  countryCode: 'CZ',
  locality: '',
  district: '',
  city: '',
  postalCode: '',
  street: '',
  streetNumber: '',
  duration: 30,
  useCompanyAddress: true,
};

export const initCreateSupplyValues: ICreateSupplyValues = {
  title: '',
  text: '',
  companyId: 1,
  categoryId: 0,
  countryCode: 'CZ',
  locality: '',
  district: '',
  city: '',
  postalCode: '',
  street: '',
  streetNumber: '',
  duration: 30,
  useCompanyAddress: true,
};
