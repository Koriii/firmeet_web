import { Select, Stack } from '@chakra-ui/react';
import { FieldInputProps } from 'formik';
import React, { SelectHTMLAttributes } from 'react';
import { ISelectOption } from '../../../../types/types';

type IDropdownProps = SelectHTMLAttributes<HTMLSelectElement> & {
  name?: string;
  label?: string;
  field?: FieldInputProps<any>;
  items: ISelectOption<string | number>[];
  placeholder?: string;
};

export const Dropdown: React.FC<IDropdownProps> = ({ items, placeholder, label, field, name, ...props }) => {
  return (
    <Stack spacing={3}>
      {field ? (
        <Select
          placeholder={placeholder}
          id={field.name}
          as="select"
          disabled={props.disabled}
          defaultValue={props.defaultValue}
          {...field}>
          {items?.map((item: any) => (
            <option key={item.value} value={item.value} label={item.label} />
          ))}
        </Select>
      ) : (
        <Select
          placeholder={placeholder}
          id={name}
          as="select"
          disabled={props.disabled}
          defaultValue={props.defaultValue}
          {...props}
          size="md">
          {items?.map((item: any) => (
            <option key={item.value} value={item.value} label={item.label} />
          ))}
        </Select>
      )}
    </Stack>
  );
};
