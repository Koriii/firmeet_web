import { Heading, VStack, Text, Box, Stack, List } from '@chakra-ui/react';
import React from 'react';
import { Button, PriceLabel, PriceWrapper, RoundedLabel } from '../../common/components/shared/atoms';
import { IconList } from '../../common/components/shared/molecules';
import { IconType } from '../../common/types/enums';
import { gray } from '../../utils/constants';

export const Pricing: React.FC = () => {
  return (
    <Box>
      <VStack spacing={2} textAlign="center">
        <Heading as="h1" fontSize="4xl">
          Plans that fit your need
        </Heading>
        <Text fontSize="lg" color={gray}>
          Start with 14-day free trial. No credit card needed. Cancel at anytime.
        </Text>
      </VStack>
      <Stack
        direction={{ base: 'column', md: 'row' }}
        textAlign="center"
        justify="center"
        spacing={{ base: 4, lg: 10 }}
        py={10}>
        <PriceWrapper>
          <Box py={4} px={12}>
            <Text fontWeight="500" fontSize="2xl">
              Hobby
            </Text>
            <PriceLabel price="79" />
          </Box>
          <VStack bg={'gray.50'} py={4} borderBottomRadius={'xl'}>
            <IconList
              iconType={IconType.CHECK}
              listItems={['unlimited build minutes', 'Lorem, ipsum dolor.', '5TB Lorem, ipsum dolor.']}
            />
            <Box w="80%" pt={7}>
              <Button w="full" variant="outline" title="Start trial" />
            </Box>
          </VStack>
        </PriceWrapper>

        <PriceWrapper>
          <Box position="relative">
            <Box position="absolute" top="-16px" left="50%" style={{ transform: 'translate(-50%)' }}>
              <RoundedLabel text="Most Popular" />
            </Box>
            <Box py={4} px={12}>
              <Text fontWeight="500" fontSize="2xl" color="gray.300">
                Comming soon...
              </Text>
              <PriceLabel price="149" />
            </Box>
            <VStack bg={'gray.50'} py={4} borderBottomRadius={'xl'}>
              <IconList
                iconType={IconType.CHECK}
                listItems={[
                  'unlimited build minutes',
                  'Lorem, ipsum dolor.',
                  '5TB Lorem, ipsum dolor.',
                  '5TB Lorem, ipsum dolor.',
                  '5TB Lorem, ipsum dolor.',
                ]}
              />
              <Box w="80%" pt={7}>
                <Button
                  w="full"
                  title="
                  Start trial"
                />
              </Box>
            </VStack>
          </Box>
        </PriceWrapper>

        <PriceWrapper>
          <Box py={4} px={12}>
            <Text fontWeight="500" fontSize="2xl" color="gray.300">
              Comming soon...
            </Text>
            <PriceLabel price="349" />
          </Box>
          <VStack bg={'gray.50'} py={4} borderBottomRadius={'xl'}>
            <List spacing={3} textAlign="start" px={12}>
              <IconList
                iconType={IconType.CHECK}
                listItems={['unlimited build minutes', 'Lorem, ipsum dolor.', '5TB Lorem, ipsum dolor.']}
              />
            </List>
            <Box w="80%" pt={6}>
              <Button
                w="full"
                variant="outline"
                title="
                Start trial"
              />
            </Box>
          </VStack>
        </PriceWrapper>
      </Stack>
    </Box>
  );
};
