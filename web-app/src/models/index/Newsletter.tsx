import { Box, Center, Text } from '@chakra-ui/layout';
import { Form, Formik } from 'formik';
import React, { useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { NewsletterSubscriptionInputModel, useSubmitNewsletterSubscriptionMutation } from '../../generated/graphql';
import { validateEmail } from '../../utils/regexes';
import { Button, DropdownField, InputField } from '../../common/components/shared/atoms';

export const Newsletter: React.FC = () => {
  const intl = useIntl();
  const [isSubscribed, setIsSubscribed] = useState(false);
  const [subscribeNewsletter] = useSubmitNewsletterSubscriptionMutation();

  const onSubmitCallback = async (values: NewsletterSubscriptionInputModel) => {
    const response = await subscribeNewsletter({ variables: { input: { email: values.email } } });
    setIsSubscribed(!!response.data?.submitNewsletterSubscription.id);
  };

  const setValidations = (data: NewsletterSubscriptionInputModel) => {
    const errors = {} as any;

    for (const [key, value] of Object.entries(data)) {
      if (!value) {
        if (key === 'email') {
          errors[key] = `Field is required`;
        }
      } else {
        if (!validateEmail(String(value))) {
          if (key === 'email') {
            errors[key] = `This is not a valid email type`;
          }
        }
      }
    }
    return errors;
  };

  return (
    <Center>
      <Box border="1px" textAlign="center" borderColor="gray.300" borderRadius="lg" p={8} width="container.md">
        <Text fontWeight="700" fontSize="2xl">
          <FormattedMessage id="Index.Newsletter.Title" defaultMessage="Join our newsletter" />
        </Text>
        <Formik
          initialValues={{ email: '', period: null, categoryIds: null }}
          onSubmit={onSubmitCallback}
          validate={(data) => setValidations(data)}>
          {({ submitForm, isSubmitting }) => (
            <Form>
              <Box mb={4}>
                {!isSubscribed ? (
                  <InputField name="email" placeholder="john@doe.com" />
                ) : (
                  <Text>
                    <FormattedMessage
                      id="Index.Newsletter.SuccessMsg"
                      defaultMessage="You have been successfuly subscribed to our newsletter!"
                    />
                  </Text>
                )}
              </Box>
              <Box float="left" display={'inline'} mr={4}>
                <DropdownField
                  name="newsletter-category"
                  items={[
                    {
                      value: 0,
                      label: `${intl.formatMessage({
                        id: 'Index.Newsletter.CategoryPlaceholder',
                        defaultMessage: 'Select category',
                      })}`,
                    },
                  ]}
                  disabled={true}
                />
              </Box>
              <Box float="left" display={'inline'}>
                <DropdownField
                  name="newsletter-period"
                  items={[
                    {
                      value: 0,
                      label: `${intl.formatMessage({
                        id: 'Index.Newsletter.PeriodPlaceholder',
                        defaultMessage: 'Once a week',
                      })}`,
                    },
                  ]}
                  disabled={true}
                />
              </Box>
              <Button
                float="right"
                title={intl.formatMessage({ id: 'Index.Newsletter.SubmitButton', defaultMessage: 'Subscribe' })}
                type="submit"
                isDisabled={isSubscribed}
                isLoading={isSubmitting}
                onSubmit={submitForm}
              />
            </Form>
          )}
        </Formik>
      </Box>
    </Center>
  );
};
