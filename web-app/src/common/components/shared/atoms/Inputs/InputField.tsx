import { FormControl, FormLabel, Input, FormErrorMessage, Textarea } from '@chakra-ui/react';
import { useField } from 'formik';
import React, { InputHTMLAttributes } from 'react';

type InputFieldProps = InputHTMLAttributes<HTMLTextAreaElement | HTMLInputElement> & {
  name: string;
  label?: string;
  textarea?: boolean;
  rows?: number;
  mt?: string;
};

// eslint-disable-next-line no-unused-vars
export const InputField: React.FC<InputFieldProps> = ({ label, size: _, textarea = false, rows = 4, mt, ...props }) => {
  const [field, { error, touched }] = useField(props);

  return (
    <FormControl isInvalid={!!error && touched} isRequired={props.required} mt={mt ?? 2}>
      {label && <FormLabel htmlFor={field.name}>{label}</FormLabel>}
      {textarea ? (
        <Textarea {...field} {...props} id={field.name} rows={rows} />
      ) : (
        <Input {...field} {...props} id={field.name} />
      )}
      {error ? <FormErrorMessage>{error}</FormErrorMessage> : null}
    </FormControl>
  );
};
