import { Tag, TagProps } from '@chakra-ui/react';
import React from 'react';
import { colorScheme } from '../../../../../utils/constants';

interface ITagProps extends TagProps {
  children: React.ReactNode;
}

export const TagComonent: React.FC<ITagProps> = ({ ...props }) => {
  return (
    <Tag size={props.size ?? 'md'} variant={props.variant ?? 'outline'} colorScheme={colorScheme} {...props}>
      {props.children}
    </Tag>
  );
};
