import { useDemandPostQuery, useSupplyPostQuery } from '../../generated/graphql';

export const useGetDemandPostById = (id: number, userId?: number) => {
  return useDemandPostQuery({
    skip: id === -1,
    variables: {
      id: id,
      userId: userId,
    },
  });
};

export const useGetSupplyPostById = (id: number, userId?: number) => {
  return useSupplyPostQuery({
    skip: id === -1,
    variables: {
      id: id,
      userId: userId,
    },
  });
};
