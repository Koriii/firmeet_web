import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
const defaultOptions = {} as const;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type Category = {
  __typename?: 'Category';
  children: Array<Category>;
  createdAt: Scalars['String'];
  id: Scalars['Float'];
  name: Scalars['String'];
};

export type Company = {
  __typename?: 'Company';
  cin?: Maybe<Scalars['String']>;
  city: Scalars['String'];
  countryCode: Scalars['String'];
  createdAt: Scalars['String'];
  creator: User;
  creatorId: Scalars['Float'];
  district: Scalars['String'];
  id: Scalars['Float'];
  isVisible: Scalars['Boolean'];
  locality: Scalars['String'];
  name: Scalars['String'];
  points: Scalars['Float'];
  postalCode: Scalars['String'];
  street: Scalars['String'];
  streetNumber: Scalars['String'];
  updatedAt: Scalars['String'];
  vat?: Maybe<Scalars['String']>;
  voteStatus?: Maybe<Scalars['Int']>;
  website?: Maybe<Scalars['String']>;
};

export type CompanyInputModel = {
  cin: Scalars['String'];
  city: Scalars['String'];
  countryCode: Scalars['String'];
  district: Scalars['String'];
  id?: Maybe<Scalars['Float']>;
  locality: Scalars['String'];
  name: Scalars['String'];
  postalCode: Scalars['String'];
  street: Scalars['String'];
  streetNumber: Scalars['String'];
  vat: Scalars['String'];
  website?: Maybe<Scalars['String']>;
};

export type Demand = {
  __typename?: 'Demand';
  category: Category;
  categoryId: Scalars['Float'];
  city: Scalars['String'];
  company: Company;
  companyId: Scalars['Float'];
  countryCode: Scalars['String'];
  createdAt: Scalars['String'];
  creator: User;
  creatorId: Scalars['Float'];
  district: Scalars['String'];
  duration: Scalars['Float'];
  id: Scalars['Float'];
  locality: Scalars['String'];
  postalCode: Scalars['String'];
  street: Scalars['String'];
  streetNumber: Scalars['String'];
  text: Scalars['String'];
  textSnippet: Scalars['String'];
  title: Scalars['String'];
  updatedAt: Scalars['String'];
  useCompanyAddress?: Maybe<Scalars['Boolean']>;
};

export type DemandFilterInputModel = {
  categoryId?: Maybe<Scalars['Float']>;
  countryCode?: Maybe<Scalars['String']>;
};

export type DemandPostInputModel = {
  categoryId: Scalars['Float'];
  city: Scalars['String'];
  companyId: Scalars['Float'];
  countryCode: Scalars['String'];
  district: Scalars['String'];
  duration: Scalars['Float'];
  locality: Scalars['String'];
  postalCode: Scalars['String'];
  street: Scalars['String'];
  streetNumber: Scalars['String'];
  text: Scalars['String'];
  title: Scalars['String'];
  useCompanyAddress?: Maybe<Scalars['Boolean']>;
};

export type EmailPasswordInputModel = {
  email: Scalars['String'];
  password: Scalars['String'];
};

export type Mutation = {
  __typename?: 'Mutation';
  changePassword: UserResponseModel;
  createCompany: Company;
  createDemandPost: Demand;
  createSupplyPost: Supply;
  deleteCompany: Scalars['Boolean'];
  deleteDemandPost: Scalars['Boolean'];
  deleteSupplyPost: Scalars['Boolean'];
  forgotPassword: Scalars['Boolean'];
  login: UserResponseModel;
  logout: Scalars['Boolean'];
  register: UserResponseModel;
  submitNewsletterSubscription: NewsletterSubscription;
  updateCompany: Company;
  updateDemandPost?: Maybe<Demand>;
  updateSupplyPost?: Maybe<Supply>;
};


export type MutationChangePasswordArgs = {
  newPassword: Scalars['String'];
  token: Scalars['String'];
};


export type MutationCreateCompanyArgs = {
  input: CompanyInputModel;
};


export type MutationCreateDemandPostArgs = {
  input: DemandPostInputModel;
};


export type MutationCreateSupplyPostArgs = {
  input: SupplyPostInputModel;
};


export type MutationDeleteCompanyArgs = {
  id: Scalars['Int'];
};


export type MutationDeleteDemandPostArgs = {
  id: Scalars['Int'];
};


export type MutationDeleteSupplyPostArgs = {
  id: Scalars['Int'];
};


export type MutationForgotPasswordArgs = {
  email: Scalars['String'];
  language: Scalars['String'];
};


export type MutationLoginArgs = {
  email: Scalars['String'];
  password: Scalars['String'];
};


export type MutationRegisterArgs = {
  options: EmailPasswordInputModel;
};


export type MutationSubmitNewsletterSubscriptionArgs = {
  input: NewsletterSubscriptionInputModel;
};


export type MutationUpdateCompanyArgs = {
  id: Scalars['Int'];
  input: CompanyInputModel;
};


export type MutationUpdateDemandPostArgs = {
  id: Scalars['Int'];
  input: DemandPostInputModel;
};


export type MutationUpdateSupplyPostArgs = {
  id: Scalars['Int'];
  input: SupplyPostInputModel;
};

export type NewsletterSubscription = {
  __typename?: 'NewsletterSubscription';
  createdAt: Scalars['String'];
  email: Scalars['String'];
  id: Scalars['Float'];
  period: Scalars['Float'];
  targetCategoryId: Scalars['Float'];
  updatedAt: Scalars['String'];
  userId: Scalars['Float'];
};

export type NewsletterSubscriptionInputModel = {
  categoryIds?: Maybe<Array<Scalars['Float']>>;
  email: Scalars['String'];
  period?: Maybe<Scalars['Float']>;
};

export type OptionValue = {
  __typename?: 'OptionValue';
  label: Scalars['String'];
  value: Scalars['Float'];
};

export type PaginatedDemandPostsModel = {
  __typename?: 'PaginatedDemandPostsModel';
  hasMore: Scalars['Boolean'];
  posts: Array<Demand>;
};

export type PaginatedSupplyPostsModel = {
  __typename?: 'PaginatedSupplyPostsModel';
  hasMore: Scalars['Boolean'];
  posts: Array<Supply>;
};

export type Query = {
  __typename?: 'Query';
  categories: Array<Category>;
  companies: Array<Company>;
  companyOptions: Array<OptionValue>;
  demandPost?: Maybe<Demand>;
  demandPosts: PaginatedDemandPostsModel;
  me?: Maybe<User>;
  supplyPost?: Maybe<Supply>;
  supplyPosts: PaginatedSupplyPostsModel;
};


export type QueryCompaniesArgs = {
  creatorId: Scalars['Float'];
};


export type QueryCompanyOptionsArgs = {
  creatorId: Scalars['Float'];
};


export type QueryDemandPostArgs = {
  id: Scalars['Int'];
  userId?: Maybe<Scalars['Int']>;
};


export type QueryDemandPostsArgs = {
  cursor?: Maybe<Scalars['String']>;
  filters?: Maybe<DemandFilterInputModel>;
  limit: Scalars['Int'];
};


export type QuerySupplyPostArgs = {
  id: Scalars['Int'];
  userId?: Maybe<Scalars['Int']>;
};


export type QuerySupplyPostsArgs = {
  cursor?: Maybe<Scalars['String']>;
  filters?: Maybe<SupplyFilterInputModel>;
  limit: Scalars['Int'];
};

export type Supply = {
  __typename?: 'Supply';
  category: Category;
  categoryId: Scalars['Float'];
  city: Scalars['String'];
  company: Company;
  companyId: Scalars['Float'];
  countryCode: Scalars['String'];
  createdAt: Scalars['String'];
  creator: User;
  creatorId: Scalars['Float'];
  district: Scalars['String'];
  duration: Scalars['Float'];
  id: Scalars['Float'];
  locality: Scalars['String'];
  postalCode: Scalars['String'];
  street: Scalars['String'];
  streetNumber: Scalars['String'];
  text: Scalars['String'];
  textSnippet: Scalars['String'];
  title: Scalars['String'];
  updatedAt: Scalars['String'];
  useCompanyAddress?: Maybe<Scalars['Boolean']>;
};

export type SupplyFilterInputModel = {
  categoryId?: Maybe<Scalars['Float']>;
  countryCode?: Maybe<Scalars['String']>;
};

export type SupplyPostInputModel = {
  categoryId: Scalars['Float'];
  city: Scalars['String'];
  companyId: Scalars['Float'];
  countryCode: Scalars['String'];
  district: Scalars['String'];
  duration: Scalars['Float'];
  locality: Scalars['String'];
  postalCode: Scalars['String'];
  street: Scalars['String'];
  streetNumber: Scalars['String'];
  text: Scalars['String'];
  title: Scalars['String'];
  useCompanyAddress?: Maybe<Scalars['Boolean']>;
};

export type User = {
  __typename?: 'User';
  createdAt: Scalars['String'];
  email: Scalars['String'];
  id: Scalars['Float'];
  updatedAt: Scalars['String'];
};

export type UserFieldError = {
  __typename?: 'UserFieldError';
  field: Scalars['String'];
  message: Scalars['String'];
};

export type UserResponseModel = {
  __typename?: 'UserResponseModel';
  errors?: Maybe<Array<UserFieldError>>;
  user?: Maybe<User>;
};

export type CategoriesQueryVariables = Exact<{ [key: string]: never; }>;


export type CategoriesQuery = { __typename?: 'Query', categories: Array<{ __typename?: 'Category', id: number, name: string, children: Array<{ __typename?: 'Category', id: number, name: string, children: Array<{ __typename?: 'Category', id: number, name: string }> }> }> };

export type GeneralCompanyFragmentFragment = { __typename?: 'Company', name: string, cin?: Maybe<string>, vat?: Maybe<string>, countryCode: string, website?: Maybe<string>, district: string, city: string, postalCode: string, street: string, streetNumber: string, locality: string };

export type CreateCompanyMutationVariables = Exact<{
  input: CompanyInputModel;
}>;


export type CreateCompanyMutation = { __typename?: 'Mutation', createCompany: { __typename?: 'Company', id: number, creatorId: number, name: string, cin?: Maybe<string>, vat?: Maybe<string>, countryCode: string, website?: Maybe<string>, district: string, city: string, postalCode: string, street: string, streetNumber: string, locality: string } };

export type DeleteCompanyMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteCompanyMutation = { __typename?: 'Mutation', deleteCompany: boolean };

export type UpdateCompanyMutationVariables = Exact<{
  id: Scalars['Int'];
  input: CompanyInputModel;
}>;


export type UpdateCompanyMutation = { __typename?: 'Mutation', updateCompany: { __typename?: 'Company', id: number, creatorId: number, name: string, cin?: Maybe<string>, vat?: Maybe<string>, countryCode: string, website?: Maybe<string>, district: string, city: string, postalCode: string, street: string, streetNumber: string, locality: string } };

export type CompaniesQueryVariables = Exact<{
  creatorId: Scalars['Float'];
}>;


export type CompaniesQuery = { __typename?: 'Query', companies: Array<{ __typename?: 'Company', id: number, creatorId: number, name: string, cin?: Maybe<string>, vat?: Maybe<string>, countryCode: string, website?: Maybe<string>, district: string, city: string, postalCode: string, street: string, streetNumber: string, locality: string }> };

export type CompanyOptionsQueryVariables = Exact<{
  creatorId: Scalars['Float'];
}>;


export type CompanyOptionsQuery = { __typename?: 'Query', companyOptions: Array<{ __typename?: 'OptionValue', value: number, label: string }> };

export type DemandMutationSnippetFragment = { __typename?: 'Demand', id: number, title: string, text: string, companyId: number, categoryId: number, countryCode: string, district: string, city: string, postalCode: string, street: string, streetNumber: string };

export type DemandTextSnippetFragment = { __typename?: 'Demand', id: number, title: string, categoryId: number, updatedAt: string, createdAt: string, creatorId: number, textSnippet: string, creator: { __typename?: 'User', id: number, email: string } };

export type CreateDemandPostMutationVariables = Exact<{
  input: DemandPostInputModel;
}>;


export type CreateDemandPostMutation = { __typename?: 'Mutation', createDemandPost: { __typename?: 'Demand', useCompanyAddress?: Maybe<boolean>, updatedAt: string, creatorId: number, createdAt: string, id: number, title: string, text: string, companyId: number, categoryId: number, countryCode: string, district: string, city: string, postalCode: string, street: string, streetNumber: string } };

export type DeleteDemandPostMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteDemandPostMutation = { __typename?: 'Mutation', deleteDemandPost: boolean };

export type DemandPostQueryVariables = Exact<{
  id: Scalars['Int'];
  userId?: Maybe<Scalars['Int']>;
}>;


export type DemandPostQuery = { __typename?: 'Query', demandPost?: Maybe<{ __typename?: 'Demand', id: number, title: string, text: string, creatorId: number, categoryId: number, companyId: number, countryCode: string, district: string, city: string, postalCode: string, street: string, streetNumber: string, updatedAt: string, createdAt: string, creator: { __typename?: 'User', id: number, email: string }, company: { __typename?: 'Company', name: string, cin?: Maybe<string>, vat?: Maybe<string>, countryCode: string, website?: Maybe<string>, district: string, city: string, postalCode: string, street: string, streetNumber: string, locality: string } }> };

export type DemandPostsQueryVariables = Exact<{
  limit: Scalars['Int'];
  cursor?: Maybe<Scalars['String']>;
  filters: DemandFilterInputModel;
}>;


export type DemandPostsQuery = { __typename?: 'Query', demandPosts: { __typename?: 'PaginatedDemandPostsModel', hasMore: boolean, posts: Array<{ __typename?: 'Demand', id: number, title: string, categoryId: number, updatedAt: string, createdAt: string, creatorId: number, textSnippet: string, creator: { __typename?: 'User', id: number, email: string } }> } };

export type SubmitNewsletterSubscriptionMutationVariables = Exact<{
  input: NewsletterSubscriptionInputModel;
}>;


export type SubmitNewsletterSubscriptionMutation = { __typename?: 'Mutation', submitNewsletterSubscription: { __typename?: 'NewsletterSubscription', id: number, email: string } };

export type SupplyMutationSnippetFragment = { __typename?: 'Supply', id: number, title: string, text: string, companyId: number, categoryId: number, countryCode: string, district: string, city: string, postalCode: string, street: string, streetNumber: string };

export type SupplyTextSnippetFragment = { __typename?: 'Supply', id: number, title: string, categoryId: number, updatedAt: string, createdAt: string, creatorId: number, textSnippet: string, creator: { __typename?: 'User', id: number, email: string } };

export type CreateSupplyPostMutationVariables = Exact<{
  input: SupplyPostInputModel;
}>;


export type CreateSupplyPostMutation = { __typename?: 'Mutation', createSupplyPost: { __typename?: 'Supply', useCompanyAddress?: Maybe<boolean>, updatedAt: string, creatorId: number, createdAt: string, id: number, title: string, text: string, companyId: number, categoryId: number, countryCode: string, district: string, city: string, postalCode: string, street: string, streetNumber: string } };

export type DeleteSupplyPostMutationVariables = Exact<{
  id: Scalars['Int'];
}>;


export type DeleteSupplyPostMutation = { __typename?: 'Mutation', deleteSupplyPost: boolean };

export type SupplyPostQueryVariables = Exact<{
  id: Scalars['Int'];
  userId?: Maybe<Scalars['Int']>;
}>;


export type SupplyPostQuery = { __typename?: 'Query', supplyPost?: Maybe<{ __typename?: 'Supply', id: number, title: string, text: string, creatorId: number, categoryId: number, companyId: number, countryCode: string, district: string, city: string, postalCode: string, street: string, streetNumber: string, updatedAt: string, createdAt: string, creator: { __typename?: 'User', id: number, email: string }, company: { __typename?: 'Company', name: string, cin?: Maybe<string>, vat?: Maybe<string>, countryCode: string, website?: Maybe<string>, district: string, city: string, postalCode: string, street: string, streetNumber: string, locality: string } }> };

export type SupplyPostsQueryVariables = Exact<{
  limit: Scalars['Int'];
  cursor?: Maybe<Scalars['String']>;
  filters: SupplyFilterInputModel;
}>;


export type SupplyPostsQuery = { __typename?: 'Query', supplyPosts: { __typename?: 'PaginatedSupplyPostsModel', hasMore: boolean, posts: Array<{ __typename?: 'Supply', id: number, title: string, categoryId: number, updatedAt: string, createdAt: string, creatorId: number, textSnippet: string, creator: { __typename?: 'User', id: number, email: string } }> } };

export type RegularErrorFragment = { __typename?: 'UserFieldError', field: string, message: string };

export type RegularUserFragment = { __typename?: 'User', id: number, email: string };

export type RegularUserResponseFragment = { __typename?: 'UserResponseModel', errors?: Maybe<Array<{ __typename?: 'UserFieldError', field: string, message: string }>>, user?: Maybe<{ __typename?: 'User', id: number, email: string }> };

export type ChangePasswordMutationVariables = Exact<{
  token: Scalars['String'];
  newPassword: Scalars['String'];
}>;


export type ChangePasswordMutation = { __typename?: 'Mutation', changePassword: { __typename?: 'UserResponseModel', errors?: Maybe<Array<{ __typename?: 'UserFieldError', field: string, message: string }>>, user?: Maybe<{ __typename?: 'User', id: number, email: string }> } };

export type ForgotPasswordMutationVariables = Exact<{
  email: Scalars['String'];
  language: Scalars['String'];
}>;


export type ForgotPasswordMutation = { __typename?: 'Mutation', forgotPassword: boolean };

export type LoginMutationVariables = Exact<{
  email: Scalars['String'];
  password: Scalars['String'];
}>;


export type LoginMutation = { __typename?: 'Mutation', login: { __typename?: 'UserResponseModel', errors?: Maybe<Array<{ __typename?: 'UserFieldError', field: string, message: string }>>, user?: Maybe<{ __typename?: 'User', id: number, email: string }> } };

export type LogoutMutationVariables = Exact<{ [key: string]: never; }>;


export type LogoutMutation = { __typename?: 'Mutation', logout: boolean };

export type RegisterMutationVariables = Exact<{
  options: EmailPasswordInputModel;
}>;


export type RegisterMutation = { __typename?: 'Mutation', register: { __typename?: 'UserResponseModel', errors?: Maybe<Array<{ __typename?: 'UserFieldError', field: string, message: string }>>, user?: Maybe<{ __typename?: 'User', id: number, email: string }> } };

export type MeQueryVariables = Exact<{ [key: string]: never; }>;


export type MeQuery = { __typename?: 'Query', me?: Maybe<{ __typename?: 'User', id: number, email: string }> };

export const GeneralCompanyFragmentFragmentDoc = gql`
    fragment GeneralCompanyFragment on Company {
  name
  cin
  vat
  countryCode
  website
  district
  city
  postalCode
  street
  streetNumber
  locality
}
    `;
export const DemandMutationSnippetFragmentDoc = gql`
    fragment DemandMutationSnippet on Demand {
  id
  title
  text
  companyId
  categoryId
  countryCode
  district
  city
  postalCode
  street
  streetNumber
}
    `;
export const DemandTextSnippetFragmentDoc = gql`
    fragment DemandTextSnippet on Demand {
  id
  title
  categoryId
  updatedAt
  createdAt
  creatorId
  textSnippet
  creator {
    id
    email
  }
}
    `;
export const SupplyMutationSnippetFragmentDoc = gql`
    fragment SupplyMutationSnippet on Supply {
  id
  title
  text
  companyId
  categoryId
  countryCode
  district
  city
  postalCode
  street
  streetNumber
}
    `;
export const SupplyTextSnippetFragmentDoc = gql`
    fragment SupplyTextSnippet on Supply {
  id
  title
  categoryId
  updatedAt
  createdAt
  creatorId
  textSnippet
  creator {
    id
    email
  }
}
    `;
export const RegularErrorFragmentDoc = gql`
    fragment RegularError on UserFieldError {
  field
  message
}
    `;
export const RegularUserFragmentDoc = gql`
    fragment RegularUser on User {
  id
  email
}
    `;
export const RegularUserResponseFragmentDoc = gql`
    fragment RegularUserResponse on UserResponseModel {
  errors {
    ...RegularError
  }
  user {
    ...RegularUser
  }
}
    ${RegularErrorFragmentDoc}
${RegularUserFragmentDoc}`;
export const CategoriesDocument = gql`
    query Categories {
  categories {
    id
    name
    children {
      id
      name
      children {
        id
        name
      }
    }
  }
}
    `;

/**
 * __useCategoriesQuery__
 *
 * To run a query within a React component, call `useCategoriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCategoriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCategoriesQuery({
 *   variables: {
 *   },
 * });
 */
export function useCategoriesQuery(baseOptions?: Apollo.QueryHookOptions<CategoriesQuery, CategoriesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CategoriesQuery, CategoriesQueryVariables>(CategoriesDocument, options);
      }
export function useCategoriesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CategoriesQuery, CategoriesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CategoriesQuery, CategoriesQueryVariables>(CategoriesDocument, options);
        }
export type CategoriesQueryHookResult = ReturnType<typeof useCategoriesQuery>;
export type CategoriesLazyQueryHookResult = ReturnType<typeof useCategoriesLazyQuery>;
export type CategoriesQueryResult = Apollo.QueryResult<CategoriesQuery, CategoriesQueryVariables>;
export const CreateCompanyDocument = gql`
    mutation CreateCompany($input: CompanyInputModel!) {
  createCompany(input: $input) {
    id
    creatorId
    ...GeneralCompanyFragment
  }
}
    ${GeneralCompanyFragmentFragmentDoc}`;
export type CreateCompanyMutationFn = Apollo.MutationFunction<CreateCompanyMutation, CreateCompanyMutationVariables>;

/**
 * __useCreateCompanyMutation__
 *
 * To run a mutation, you first call `useCreateCompanyMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateCompanyMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createCompanyMutation, { data, loading, error }] = useCreateCompanyMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateCompanyMutation(baseOptions?: Apollo.MutationHookOptions<CreateCompanyMutation, CreateCompanyMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateCompanyMutation, CreateCompanyMutationVariables>(CreateCompanyDocument, options);
      }
export type CreateCompanyMutationHookResult = ReturnType<typeof useCreateCompanyMutation>;
export type CreateCompanyMutationResult = Apollo.MutationResult<CreateCompanyMutation>;
export type CreateCompanyMutationOptions = Apollo.BaseMutationOptions<CreateCompanyMutation, CreateCompanyMutationVariables>;
export const DeleteCompanyDocument = gql`
    mutation DeleteCompany($id: Int!) {
  deleteCompany(id: $id)
}
    `;
export type DeleteCompanyMutationFn = Apollo.MutationFunction<DeleteCompanyMutation, DeleteCompanyMutationVariables>;

/**
 * __useDeleteCompanyMutation__
 *
 * To run a mutation, you first call `useDeleteCompanyMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteCompanyMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteCompanyMutation, { data, loading, error }] = useDeleteCompanyMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteCompanyMutation(baseOptions?: Apollo.MutationHookOptions<DeleteCompanyMutation, DeleteCompanyMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteCompanyMutation, DeleteCompanyMutationVariables>(DeleteCompanyDocument, options);
      }
export type DeleteCompanyMutationHookResult = ReturnType<typeof useDeleteCompanyMutation>;
export type DeleteCompanyMutationResult = Apollo.MutationResult<DeleteCompanyMutation>;
export type DeleteCompanyMutationOptions = Apollo.BaseMutationOptions<DeleteCompanyMutation, DeleteCompanyMutationVariables>;
export const UpdateCompanyDocument = gql`
    mutation UpdateCompany($id: Int!, $input: CompanyInputModel!) {
  updateCompany(id: $id, input: $input) {
    id
    creatorId
    ...GeneralCompanyFragment
  }
}
    ${GeneralCompanyFragmentFragmentDoc}`;
export type UpdateCompanyMutationFn = Apollo.MutationFunction<UpdateCompanyMutation, UpdateCompanyMutationVariables>;

/**
 * __useUpdateCompanyMutation__
 *
 * To run a mutation, you first call `useUpdateCompanyMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useUpdateCompanyMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [updateCompanyMutation, { data, loading, error }] = useUpdateCompanyMutation({
 *   variables: {
 *      id: // value for 'id'
 *      input: // value for 'input'
 *   },
 * });
 */
export function useUpdateCompanyMutation(baseOptions?: Apollo.MutationHookOptions<UpdateCompanyMutation, UpdateCompanyMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<UpdateCompanyMutation, UpdateCompanyMutationVariables>(UpdateCompanyDocument, options);
      }
export type UpdateCompanyMutationHookResult = ReturnType<typeof useUpdateCompanyMutation>;
export type UpdateCompanyMutationResult = Apollo.MutationResult<UpdateCompanyMutation>;
export type UpdateCompanyMutationOptions = Apollo.BaseMutationOptions<UpdateCompanyMutation, UpdateCompanyMutationVariables>;
export const CompaniesDocument = gql`
    query Companies($creatorId: Float!) {
  companies(creatorId: $creatorId) {
    id
    creatorId
    ...GeneralCompanyFragment
  }
}
    ${GeneralCompanyFragmentFragmentDoc}`;

/**
 * __useCompaniesQuery__
 *
 * To run a query within a React component, call `useCompaniesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCompaniesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCompaniesQuery({
 *   variables: {
 *      creatorId: // value for 'creatorId'
 *   },
 * });
 */
export function useCompaniesQuery(baseOptions: Apollo.QueryHookOptions<CompaniesQuery, CompaniesQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CompaniesQuery, CompaniesQueryVariables>(CompaniesDocument, options);
      }
export function useCompaniesLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CompaniesQuery, CompaniesQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CompaniesQuery, CompaniesQueryVariables>(CompaniesDocument, options);
        }
export type CompaniesQueryHookResult = ReturnType<typeof useCompaniesQuery>;
export type CompaniesLazyQueryHookResult = ReturnType<typeof useCompaniesLazyQuery>;
export type CompaniesQueryResult = Apollo.QueryResult<CompaniesQuery, CompaniesQueryVariables>;
export const CompanyOptionsDocument = gql`
    query CompanyOptions($creatorId: Float!) {
  companyOptions(creatorId: $creatorId) {
    value
    label
  }
}
    `;

/**
 * __useCompanyOptionsQuery__
 *
 * To run a query within a React component, call `useCompanyOptionsQuery` and pass it any options that fit your needs.
 * When your component renders, `useCompanyOptionsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useCompanyOptionsQuery({
 *   variables: {
 *      creatorId: // value for 'creatorId'
 *   },
 * });
 */
export function useCompanyOptionsQuery(baseOptions: Apollo.QueryHookOptions<CompanyOptionsQuery, CompanyOptionsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<CompanyOptionsQuery, CompanyOptionsQueryVariables>(CompanyOptionsDocument, options);
      }
export function useCompanyOptionsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<CompanyOptionsQuery, CompanyOptionsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<CompanyOptionsQuery, CompanyOptionsQueryVariables>(CompanyOptionsDocument, options);
        }
export type CompanyOptionsQueryHookResult = ReturnType<typeof useCompanyOptionsQuery>;
export type CompanyOptionsLazyQueryHookResult = ReturnType<typeof useCompanyOptionsLazyQuery>;
export type CompanyOptionsQueryResult = Apollo.QueryResult<CompanyOptionsQuery, CompanyOptionsQueryVariables>;
export const CreateDemandPostDocument = gql`
    mutation CreateDemandPost($input: DemandPostInputModel!) {
  createDemandPost(input: $input) {
    ...DemandMutationSnippet
    useCompanyAddress
    updatedAt
    creatorId
    createdAt
  }
}
    ${DemandMutationSnippetFragmentDoc}`;
export type CreateDemandPostMutationFn = Apollo.MutationFunction<CreateDemandPostMutation, CreateDemandPostMutationVariables>;

/**
 * __useCreateDemandPostMutation__
 *
 * To run a mutation, you first call `useCreateDemandPostMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateDemandPostMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createDemandPostMutation, { data, loading, error }] = useCreateDemandPostMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateDemandPostMutation(baseOptions?: Apollo.MutationHookOptions<CreateDemandPostMutation, CreateDemandPostMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateDemandPostMutation, CreateDemandPostMutationVariables>(CreateDemandPostDocument, options);
      }
export type CreateDemandPostMutationHookResult = ReturnType<typeof useCreateDemandPostMutation>;
export type CreateDemandPostMutationResult = Apollo.MutationResult<CreateDemandPostMutation>;
export type CreateDemandPostMutationOptions = Apollo.BaseMutationOptions<CreateDemandPostMutation, CreateDemandPostMutationVariables>;
export const DeleteDemandPostDocument = gql`
    mutation DeleteDemandPost($id: Int!) {
  deleteDemandPost(id: $id)
}
    `;
export type DeleteDemandPostMutationFn = Apollo.MutationFunction<DeleteDemandPostMutation, DeleteDemandPostMutationVariables>;

/**
 * __useDeleteDemandPostMutation__
 *
 * To run a mutation, you first call `useDeleteDemandPostMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteDemandPostMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteDemandPostMutation, { data, loading, error }] = useDeleteDemandPostMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteDemandPostMutation(baseOptions?: Apollo.MutationHookOptions<DeleteDemandPostMutation, DeleteDemandPostMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteDemandPostMutation, DeleteDemandPostMutationVariables>(DeleteDemandPostDocument, options);
      }
export type DeleteDemandPostMutationHookResult = ReturnType<typeof useDeleteDemandPostMutation>;
export type DeleteDemandPostMutationResult = Apollo.MutationResult<DeleteDemandPostMutation>;
export type DeleteDemandPostMutationOptions = Apollo.BaseMutationOptions<DeleteDemandPostMutation, DeleteDemandPostMutationVariables>;
export const DemandPostDocument = gql`
    query DemandPost($id: Int!, $userId: Int) {
  demandPost(id: $id, userId: $userId) {
    id
    title
    text
    creatorId
    categoryId
    creator {
      id
      email
    }
    companyId
    company {
      ...GeneralCompanyFragment
    }
    countryCode
    district
    city
    postalCode
    street
    streetNumber
    updatedAt
    createdAt
  }
}
    ${GeneralCompanyFragmentFragmentDoc}`;

/**
 * __useDemandPostQuery__
 *
 * To run a query within a React component, call `useDemandPostQuery` and pass it any options that fit your needs.
 * When your component renders, `useDemandPostQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useDemandPostQuery({
 *   variables: {
 *      id: // value for 'id'
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useDemandPostQuery(baseOptions: Apollo.QueryHookOptions<DemandPostQuery, DemandPostQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<DemandPostQuery, DemandPostQueryVariables>(DemandPostDocument, options);
      }
export function useDemandPostLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<DemandPostQuery, DemandPostQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<DemandPostQuery, DemandPostQueryVariables>(DemandPostDocument, options);
        }
export type DemandPostQueryHookResult = ReturnType<typeof useDemandPostQuery>;
export type DemandPostLazyQueryHookResult = ReturnType<typeof useDemandPostLazyQuery>;
export type DemandPostQueryResult = Apollo.QueryResult<DemandPostQuery, DemandPostQueryVariables>;
export const DemandPostsDocument = gql`
    query DemandPosts($limit: Int!, $cursor: String, $filters: DemandFilterInputModel!) {
  demandPosts(limit: $limit, cursor: $cursor, filters: $filters) {
    hasMore
    posts {
      ...DemandTextSnippet
    }
  }
}
    ${DemandTextSnippetFragmentDoc}`;

/**
 * __useDemandPostsQuery__
 *
 * To run a query within a React component, call `useDemandPostsQuery` and pass it any options that fit your needs.
 * When your component renders, `useDemandPostsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useDemandPostsQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      cursor: // value for 'cursor'
 *      filters: // value for 'filters'
 *   },
 * });
 */
export function useDemandPostsQuery(baseOptions: Apollo.QueryHookOptions<DemandPostsQuery, DemandPostsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<DemandPostsQuery, DemandPostsQueryVariables>(DemandPostsDocument, options);
      }
export function useDemandPostsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<DemandPostsQuery, DemandPostsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<DemandPostsQuery, DemandPostsQueryVariables>(DemandPostsDocument, options);
        }
export type DemandPostsQueryHookResult = ReturnType<typeof useDemandPostsQuery>;
export type DemandPostsLazyQueryHookResult = ReturnType<typeof useDemandPostsLazyQuery>;
export type DemandPostsQueryResult = Apollo.QueryResult<DemandPostsQuery, DemandPostsQueryVariables>;
export const SubmitNewsletterSubscriptionDocument = gql`
    mutation SubmitNewsletterSubscription($input: NewsletterSubscriptionInputModel!) {
  submitNewsletterSubscription(input: $input) {
    id
    email
  }
}
    `;
export type SubmitNewsletterSubscriptionMutationFn = Apollo.MutationFunction<SubmitNewsletterSubscriptionMutation, SubmitNewsletterSubscriptionMutationVariables>;

/**
 * __useSubmitNewsletterSubscriptionMutation__
 *
 * To run a mutation, you first call `useSubmitNewsletterSubscriptionMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useSubmitNewsletterSubscriptionMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [submitNewsletterSubscriptionMutation, { data, loading, error }] = useSubmitNewsletterSubscriptionMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useSubmitNewsletterSubscriptionMutation(baseOptions?: Apollo.MutationHookOptions<SubmitNewsletterSubscriptionMutation, SubmitNewsletterSubscriptionMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<SubmitNewsletterSubscriptionMutation, SubmitNewsletterSubscriptionMutationVariables>(SubmitNewsletterSubscriptionDocument, options);
      }
export type SubmitNewsletterSubscriptionMutationHookResult = ReturnType<typeof useSubmitNewsletterSubscriptionMutation>;
export type SubmitNewsletterSubscriptionMutationResult = Apollo.MutationResult<SubmitNewsletterSubscriptionMutation>;
export type SubmitNewsletterSubscriptionMutationOptions = Apollo.BaseMutationOptions<SubmitNewsletterSubscriptionMutation, SubmitNewsletterSubscriptionMutationVariables>;
export const CreateSupplyPostDocument = gql`
    mutation CreateSupplyPost($input: SupplyPostInputModel!) {
  createSupplyPost(input: $input) {
    ...SupplyMutationSnippet
    useCompanyAddress
    updatedAt
    creatorId
    createdAt
  }
}
    ${SupplyMutationSnippetFragmentDoc}`;
export type CreateSupplyPostMutationFn = Apollo.MutationFunction<CreateSupplyPostMutation, CreateSupplyPostMutationVariables>;

/**
 * __useCreateSupplyPostMutation__
 *
 * To run a mutation, you first call `useCreateSupplyPostMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useCreateSupplyPostMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [createSupplyPostMutation, { data, loading, error }] = useCreateSupplyPostMutation({
 *   variables: {
 *      input: // value for 'input'
 *   },
 * });
 */
export function useCreateSupplyPostMutation(baseOptions?: Apollo.MutationHookOptions<CreateSupplyPostMutation, CreateSupplyPostMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<CreateSupplyPostMutation, CreateSupplyPostMutationVariables>(CreateSupplyPostDocument, options);
      }
export type CreateSupplyPostMutationHookResult = ReturnType<typeof useCreateSupplyPostMutation>;
export type CreateSupplyPostMutationResult = Apollo.MutationResult<CreateSupplyPostMutation>;
export type CreateSupplyPostMutationOptions = Apollo.BaseMutationOptions<CreateSupplyPostMutation, CreateSupplyPostMutationVariables>;
export const DeleteSupplyPostDocument = gql`
    mutation DeleteSupplyPost($id: Int!) {
  deleteSupplyPost(id: $id)
}
    `;
export type DeleteSupplyPostMutationFn = Apollo.MutationFunction<DeleteSupplyPostMutation, DeleteSupplyPostMutationVariables>;

/**
 * __useDeleteSupplyPostMutation__
 *
 * To run a mutation, you first call `useDeleteSupplyPostMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteSupplyPostMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteSupplyPostMutation, { data, loading, error }] = useDeleteSupplyPostMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteSupplyPostMutation(baseOptions?: Apollo.MutationHookOptions<DeleteSupplyPostMutation, DeleteSupplyPostMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<DeleteSupplyPostMutation, DeleteSupplyPostMutationVariables>(DeleteSupplyPostDocument, options);
      }
export type DeleteSupplyPostMutationHookResult = ReturnType<typeof useDeleteSupplyPostMutation>;
export type DeleteSupplyPostMutationResult = Apollo.MutationResult<DeleteSupplyPostMutation>;
export type DeleteSupplyPostMutationOptions = Apollo.BaseMutationOptions<DeleteSupplyPostMutation, DeleteSupplyPostMutationVariables>;
export const SupplyPostDocument = gql`
    query SupplyPost($id: Int!, $userId: Int) {
  supplyPost(id: $id, userId: $userId) {
    id
    title
    text
    creatorId
    categoryId
    creator {
      id
      email
    }
    companyId
    company {
      ...GeneralCompanyFragment
    }
    countryCode
    district
    city
    postalCode
    street
    streetNumber
    updatedAt
    createdAt
  }
}
    ${GeneralCompanyFragmentFragmentDoc}`;

/**
 * __useSupplyPostQuery__
 *
 * To run a query within a React component, call `useSupplyPostQuery` and pass it any options that fit your needs.
 * When your component renders, `useSupplyPostQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSupplyPostQuery({
 *   variables: {
 *      id: // value for 'id'
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useSupplyPostQuery(baseOptions: Apollo.QueryHookOptions<SupplyPostQuery, SupplyPostQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SupplyPostQuery, SupplyPostQueryVariables>(SupplyPostDocument, options);
      }
export function useSupplyPostLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SupplyPostQuery, SupplyPostQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SupplyPostQuery, SupplyPostQueryVariables>(SupplyPostDocument, options);
        }
export type SupplyPostQueryHookResult = ReturnType<typeof useSupplyPostQuery>;
export type SupplyPostLazyQueryHookResult = ReturnType<typeof useSupplyPostLazyQuery>;
export type SupplyPostQueryResult = Apollo.QueryResult<SupplyPostQuery, SupplyPostQueryVariables>;
export const SupplyPostsDocument = gql`
    query SupplyPosts($limit: Int!, $cursor: String, $filters: SupplyFilterInputModel!) {
  supplyPosts(limit: $limit, cursor: $cursor, filters: $filters) {
    hasMore
    posts {
      ...SupplyTextSnippet
    }
  }
}
    ${SupplyTextSnippetFragmentDoc}`;

/**
 * __useSupplyPostsQuery__
 *
 * To run a query within a React component, call `useSupplyPostsQuery` and pass it any options that fit your needs.
 * When your component renders, `useSupplyPostsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useSupplyPostsQuery({
 *   variables: {
 *      limit: // value for 'limit'
 *      cursor: // value for 'cursor'
 *      filters: // value for 'filters'
 *   },
 * });
 */
export function useSupplyPostsQuery(baseOptions: Apollo.QueryHookOptions<SupplyPostsQuery, SupplyPostsQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<SupplyPostsQuery, SupplyPostsQueryVariables>(SupplyPostsDocument, options);
      }
export function useSupplyPostsLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<SupplyPostsQuery, SupplyPostsQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<SupplyPostsQuery, SupplyPostsQueryVariables>(SupplyPostsDocument, options);
        }
export type SupplyPostsQueryHookResult = ReturnType<typeof useSupplyPostsQuery>;
export type SupplyPostsLazyQueryHookResult = ReturnType<typeof useSupplyPostsLazyQuery>;
export type SupplyPostsQueryResult = Apollo.QueryResult<SupplyPostsQuery, SupplyPostsQueryVariables>;
export const ChangePasswordDocument = gql`
    mutation ChangePassword($token: String!, $newPassword: String!) {
  changePassword(token: $token, newPassword: $newPassword) {
    ...RegularUserResponse
  }
}
    ${RegularUserResponseFragmentDoc}`;
export type ChangePasswordMutationFn = Apollo.MutationFunction<ChangePasswordMutation, ChangePasswordMutationVariables>;

/**
 * __useChangePasswordMutation__
 *
 * To run a mutation, you first call `useChangePasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useChangePasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [changePasswordMutation, { data, loading, error }] = useChangePasswordMutation({
 *   variables: {
 *      token: // value for 'token'
 *      newPassword: // value for 'newPassword'
 *   },
 * });
 */
export function useChangePasswordMutation(baseOptions?: Apollo.MutationHookOptions<ChangePasswordMutation, ChangePasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ChangePasswordMutation, ChangePasswordMutationVariables>(ChangePasswordDocument, options);
      }
export type ChangePasswordMutationHookResult = ReturnType<typeof useChangePasswordMutation>;
export type ChangePasswordMutationResult = Apollo.MutationResult<ChangePasswordMutation>;
export type ChangePasswordMutationOptions = Apollo.BaseMutationOptions<ChangePasswordMutation, ChangePasswordMutationVariables>;
export const ForgotPasswordDocument = gql`
    mutation ForgotPassword($email: String!, $language: String!) {
  forgotPassword(email: $email, language: $language)
}
    `;
export type ForgotPasswordMutationFn = Apollo.MutationFunction<ForgotPasswordMutation, ForgotPasswordMutationVariables>;

/**
 * __useForgotPasswordMutation__
 *
 * To run a mutation, you first call `useForgotPasswordMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useForgotPasswordMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [forgotPasswordMutation, { data, loading, error }] = useForgotPasswordMutation({
 *   variables: {
 *      email: // value for 'email'
 *      language: // value for 'language'
 *   },
 * });
 */
export function useForgotPasswordMutation(baseOptions?: Apollo.MutationHookOptions<ForgotPasswordMutation, ForgotPasswordMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<ForgotPasswordMutation, ForgotPasswordMutationVariables>(ForgotPasswordDocument, options);
      }
export type ForgotPasswordMutationHookResult = ReturnType<typeof useForgotPasswordMutation>;
export type ForgotPasswordMutationResult = Apollo.MutationResult<ForgotPasswordMutation>;
export type ForgotPasswordMutationOptions = Apollo.BaseMutationOptions<ForgotPasswordMutation, ForgotPasswordMutationVariables>;
export const LoginDocument = gql`
    mutation Login($email: String!, $password: String!) {
  login(email: $email, password: $password) {
    ...RegularUserResponse
  }
}
    ${RegularUserResponseFragmentDoc}`;
export type LoginMutationFn = Apollo.MutationFunction<LoginMutation, LoginMutationVariables>;

/**
 * __useLoginMutation__
 *
 * To run a mutation, you first call `useLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [loginMutation, { data, loading, error }] = useLoginMutation({
 *   variables: {
 *      email: // value for 'email'
 *      password: // value for 'password'
 *   },
 * });
 */
export function useLoginMutation(baseOptions?: Apollo.MutationHookOptions<LoginMutation, LoginMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LoginMutation, LoginMutationVariables>(LoginDocument, options);
      }
export type LoginMutationHookResult = ReturnType<typeof useLoginMutation>;
export type LoginMutationResult = Apollo.MutationResult<LoginMutation>;
export type LoginMutationOptions = Apollo.BaseMutationOptions<LoginMutation, LoginMutationVariables>;
export const LogoutDocument = gql`
    mutation Logout {
  logout
}
    `;
export type LogoutMutationFn = Apollo.MutationFunction<LogoutMutation, LogoutMutationVariables>;

/**
 * __useLogoutMutation__
 *
 * To run a mutation, you first call `useLogoutMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useLogoutMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [logoutMutation, { data, loading, error }] = useLogoutMutation({
 *   variables: {
 *   },
 * });
 */
export function useLogoutMutation(baseOptions?: Apollo.MutationHookOptions<LogoutMutation, LogoutMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<LogoutMutation, LogoutMutationVariables>(LogoutDocument, options);
      }
export type LogoutMutationHookResult = ReturnType<typeof useLogoutMutation>;
export type LogoutMutationResult = Apollo.MutationResult<LogoutMutation>;
export type LogoutMutationOptions = Apollo.BaseMutationOptions<LogoutMutation, LogoutMutationVariables>;
export const RegisterDocument = gql`
    mutation Register($options: EmailPasswordInputModel!) {
  register(options: $options) {
    ...RegularUserResponse
  }
}
    ${RegularUserResponseFragmentDoc}`;
export type RegisterMutationFn = Apollo.MutationFunction<RegisterMutation, RegisterMutationVariables>;

/**
 * __useRegisterMutation__
 *
 * To run a mutation, you first call `useRegisterMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRegisterMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [registerMutation, { data, loading, error }] = useRegisterMutation({
 *   variables: {
 *      options: // value for 'options'
 *   },
 * });
 */
export function useRegisterMutation(baseOptions?: Apollo.MutationHookOptions<RegisterMutation, RegisterMutationVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useMutation<RegisterMutation, RegisterMutationVariables>(RegisterDocument, options);
      }
export type RegisterMutationHookResult = ReturnType<typeof useRegisterMutation>;
export type RegisterMutationResult = Apollo.MutationResult<RegisterMutation>;
export type RegisterMutationOptions = Apollo.BaseMutationOptions<RegisterMutation, RegisterMutationVariables>;
export const MeDocument = gql`
    query Me {
  me {
    ...RegularUser
  }
}
    ${RegularUserFragmentDoc}`;

/**
 * __useMeQuery__
 *
 * To run a query within a React component, call `useMeQuery` and pass it any options that fit your needs.
 * When your component renders, `useMeQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useMeQuery({
 *   variables: {
 *   },
 * });
 */
export function useMeQuery(baseOptions?: Apollo.QueryHookOptions<MeQuery, MeQueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<MeQuery, MeQueryVariables>(MeDocument, options);
      }
export function useMeLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<MeQuery, MeQueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<MeQuery, MeQueryVariables>(MeDocument, options);
        }
export type MeQueryHookResult = ReturnType<typeof useMeQuery>;
export type MeLazyQueryHookResult = ReturnType<typeof useMeLazyQuery>;
export type MeQueryResult = Apollo.QueryResult<MeQuery, MeQueryVariables>;