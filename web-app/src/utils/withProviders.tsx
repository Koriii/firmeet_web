import * as React from 'react';

export const withProviders =
  <P extends {}>(...providers: React.ComponentType[]) =>
  (Component: React.ComponentType<P>) =>
  (props: P) => {
    function mountProviders(providersArray: React.ComponentType[]) {
      if (!providersArray.length) {
        return <Component {...props} />;
      }

      const WrapperProvider = providersArray.shift();
      if (WrapperProvider !== undefined) {
        return <WrapperProvider>{mountProviders(providersArray)}</WrapperProvider>;
      }
      return <Component {...props} />;
    }

    return mountProviders([...providers]);
  };
