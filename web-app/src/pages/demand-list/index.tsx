import { Box, Flex, Grid, Heading, Spinner, Text } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { LayoutComponent } from '../../common/components';
import { BodyContainer, Button, PostsGridContainer } from '../../common/components/shared/atoms';
import { PostCard } from '../../common/components/shared/molecules';
import { useCategoriesQuery, useDemandPostsQuery } from '../../generated/graphql';
import { PostsFilter } from '../../models/posts/filters/PostsFilter';
import { findAnythingById } from '../../models/posts/utils';
import { withApollo } from '../../utils/withApollo';

interface IFilterValue {
  categoryId: number | null;
  countryCode: string;
}

const initFilterValues = { categoryId: null, countryCode: 'CZ' };

const DemandList: React.FC = () => {
  const router = useRouter();
  const intl = useIntl();
  const [filters, setFilters] = useState<IFilterValue>(initFilterValues);

  const { data, error, loading, fetchMore, variables, refetch } = useDemandPostsQuery({
    variables: {
      limit: 10,
      cursor: null,
      filters: {
        categoryId: Number(router.query.categoryId) ?? filters.categoryId,
        countryCode: String(router.query.categoryId) ?? filters.countryCode,
      },
    },
    notifyOnNetworkStatusChange: true,
  });
  const { data: categoryData } = useCategoriesQuery();

  useEffect(() => {
    refetch({ filters: { ...initFilterValues } });
  }, [refetch]);

  // useEffect(() => {
  //   const routerCategoryId = Number(router.query.categoryId);
  //   const routerCountryCode = String(router.query.countryCode);

  //   if (routerCategoryId) {
  //     setFilters((prev) => ({
  //       ...prev,
  //       categoryId: routerCategoryId,
  //       countryCode: routerCountryCode ?? filters.countryCode,
  //     }));
  //   }
  //   refetch({ filters: { categoryId: filters.categoryId, countryCode: filters.countryCode } });
  // }, [filters.categoryId, filters.countryCode, router.query.categoryId, router.query.countryCode, refetch]);

  const setFiltersCallback = (filters: IFilterValue) => {
    router.push({ query: { categoryId: filters.categoryId, countryCode: filters.countryCode } });
    for (const [key, value] of Object.entries(filters)) {
      setFilters((prev) => ({ ...prev, [key]: value }));
    }

    refetch({ filters: { categoryId: filters.categoryId, countryCode: filters.countryCode } });
  };

  const getCategoryName = (categoryId: number) => {
    if (!categoryData) return;
    const foundCategory = findAnythingById(categoryData.categories, categoryId);
    if (foundCategory) {
      return foundCategory.name;
    }
  };

  const loadMoreCallback = () => {
    if (!data) return;
    const cursorItem = data.demandPosts.posts[data.demandPosts.posts.length - 1];
    fetchMore({
      variables: {
        limit: variables?.limit,
        cursor: cursorItem.createdAt,
        filters: { categoryId: filters.categoryId, countryCode: filters.countryCode },
      },
    });
  };

  if (!loading && !data) {
    return (
      <Flex>
        <Heading m="auto">
          <FormattedMessage id="Error.Message" defaultMessage="Oops, something went terribly wrong" />
        </Heading>
        <Text>{error?.message} </Text>
      </Flex>
    );
  }

  return (
    <LayoutComponent>
      <PostsGridContainer>
        <PostsFilter filters={filters} setFilters={setFiltersCallback} />

        <BodyContainer width={'100%'}>
          <Flex align="center">
            <Heading>
              <FormattedMessage id="Demand.List.Title" defaultMessage="List of demand offers" />
            </Heading>
            <Button
              href="/create-demand"
              ml="auto"
              title={intl.formatMessage({ id: 'Demand.List.CreateButton', defaultMessage: 'Create demand' })}
            />
          </Flex>
          <br />
          {!data && loading ? (
            <Box textAlign="center">
              <Spinner />
            </Box>
          ) : (
            <Grid templateColumns={['repeat(1, 1fr)', 'repeat(1, 1fr)', 'repeat(2, 1fr)', 'repeat(3, 1fr)']} gap={6}>
              {data &&
                data.demandPosts.posts?.map((post) =>
                  !post ? null : (
                    <PostCard
                      href={'/demand-list/post/[id]'}
                      as={`/demand-list/post/${post.id}`}
                      key={`${post.id}_${post.createdAt}`}
                      id={post.id}
                      title={post.title}
                      date={post.createdAt}
                      tags={[getCategoryName(post.categoryId)]}>
                      {post.textSnippet}
                    </PostCard>
                  ),
                )}
            </Grid>
          )}
          {data && (
            <Flex>
              <Button
                disabled={!data.demandPosts.hasMore}
                isLoading={loading}
                m="auto"
                my={8}
                variant="outline"
                onClick={loadMoreCallback}
                title={intl.formatMessage({ id: 'Demand.List.LoadMoreButton', defaultMessage: 'Load more' })}
              />
            </Flex>
          )}
        </BodyContainer>
      </PostsGridContainer>
    </LayoutComponent>
  );
};

export default withApollo({ ssr: true })(DemandList);
