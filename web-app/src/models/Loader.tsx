import { Spinner } from '@chakra-ui/react';
import React from 'react';

export const Loader: React.FC = () => {
  return <Spinner />;
};
