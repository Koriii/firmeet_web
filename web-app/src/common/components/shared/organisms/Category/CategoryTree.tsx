import React from 'react';
import { Box } from '@chakra-ui/layout';
import { CategoryCard } from '../../molecules';
import { ICategoryTreeElement } from '../../../../../models/posts/models';

export interface ICategoryTreeProps {
  categories: ICategoryTreeElement[] | null;
  onClick: (id: ICategoryTreeElement, expand: boolean) => void;
  selectedCategoryId: number | null;
  isWrapped?: boolean;
}

export const CategoryTree: React.FC<ICategoryTreeProps> = ({ categories, onClick, isWrapped, selectedCategoryId }) => {
  const depthMargin = 1;

  return (
    <Box>
      {categories?.map((category) => (
        <CategoryTreeElement
          key={category.name}
          categoryElement={category}
          onClick={onClick}
          depthMargin={depthMargin}
          isWrapped={isWrapped}
          selectedCategoryId={selectedCategoryId}
        />
      ))}
    </Box>
  );
};

export interface ICategoryTreeElementProps {
  categoryElement: ICategoryTreeElement;
  onClick: (id: ICategoryTreeElement, expand: boolean) => void;
  selectedCategoryId: number | null;
  depthMargin: number;
  isWrapped?: boolean;
}

const CategoryTreeElement: React.FC<ICategoryTreeElementProps> = ({
  categoryElement,
  onClick,
  selectedCategoryId,
  depthMargin,
  isWrapped,
}) => {
  return (
    <>
      {categoryElement.children?.length > 0 ? (
        <>
          <CategoryCard
            name={categoryElement.name}
            isExpanded={categoryElement.isExpanded}
            isActive={categoryElement.id === selectedCategoryId}
            hasChildren={true}
            depthMargin={depthMargin}
            isWrapped={isWrapped}
            onClick={(expand) => onClick(categoryElement, expand)}
            isHighlighted={categoryElement.isHighlighted}
          />
          {categoryElement.isExpanded &&
            categoryElement.children?.map((category) => (
              <CategoryTreeElement
                key={category.name}
                categoryElement={category}
                depthMargin={depthMargin * 2}
                selectedCategoryId={selectedCategoryId}
                isWrapped={isWrapped}
                onClick={onClick}
              />
            ))}
        </>
      ) : (
        <CategoryCard
          name={categoryElement.name}
          hasChildren={false}
          isExpanded={categoryElement.isExpanded}
          isActive={categoryElement.id === selectedCategoryId}
          depthMargin={depthMargin}
          isWrapped={isWrapped}
          onClick={(expand) => onClick(categoryElement, expand)}
          isHighlighted={categoryElement.isHighlighted}
        />
      )}
    </>
  );
};
