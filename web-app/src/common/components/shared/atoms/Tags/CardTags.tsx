import { Box, HStack, SpaceProps } from '@chakra-ui/react';
import React from 'react';
import { Tag } from '..';

interface ICardTagsProps {
  tags: Array<string>;
  marginTop?: SpaceProps['marginTop'];
}

export const CardTags: React.FC<ICardTagsProps> = ({ tags, marginTop }) => {
  return (
    <HStack spacing={2} marginTop={marginTop}>
      {tags.map((tag) => (
        <Box key={tag}>
          <Tag>{tag}</Tag>
        </Box>
      ))}
    </HStack>
  );
};
