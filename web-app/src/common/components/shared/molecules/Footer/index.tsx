import React from 'react';
import { Box, Container, Flex, Link, SimpleGrid, Stack, Text } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import { FormattedMessage } from 'react-intl';

export const FooterComponent: React.FC = () => {
  const { locale, locales, asPath } = useRouter();

  return (
    <Box as="footer" bg={'gray.50'} color={'gray.700'}>
      <Container as={Stack} maxW={'container.xl'} py={6}>
        <SimpleGrid templateColumns={{ sm: '1fr 1fr', md: '2fr 1fr 1fr 1fr 1fr' }} spacing={8}>
          <Stack spacing={6}>
            <Box cursor="pointer">LOGO</Box>
            <Text fontSize={'sm'}>© 2022 Firmeet. All rights reserved</Text>
          </Stack>
          <Stack align={'flex-start'}>
            <Text fontSize="lg">
              <FormattedMessage id="Footer.Links.ProductHeading" defaultMessage="Product" />
            </Text>
            <Link href={'supply-list'}>
              <FormattedMessage id="Footer.Links.Supply" defaultMessage="Supply offers" />
            </Link>
            <Link href={'demand-list'}>
              <FormattedMessage id="Footer.Links.Demand" defaultMessage="Demand offers" />
            </Link>
          </Stack>
          <Stack align={'flex-start'}>
            <Text fontSize="lg">
              <FormattedMessage id="Footer.Link.CompanyHeading" defaultMessage="Company" />
            </Text>
            <Link href={'#'}>
              <FormattedMessage id="Fotter.Links.About" defaultMessage="About" />
            </Link>
            <Link href={'#'}>
              <FormattedMessage id="Fotter.Links.Goal" defaultMessage="Goal" />
            </Link>
          </Stack>
          <Stack align={'flex-start'}>
            <Text fontSize="lg">
              <FormattedMessage id="Footer.Link.SupportHeading" defaultMessage="Support" />
            </Text>
            <Link href={'#'}>
              <FormattedMessage id="Fotter.Links.ToS" defaultMessage="Terms of Service" />
            </Link>
            <Link href={'#'}>
              <FormattedMessage id="Fotter.Links.GDPR" defaultMessage="GDPR" />
            </Link>
          </Stack>
        </SimpleGrid>
        <Flex>
          {locales &&
            locales.map((localeItem) => (
              <Text key={localeItem} mr={4} decoration={localeItem === locale ? 'underline' : ''} cursor={'pointer'}>
                <NextLink href={asPath} locale={localeItem}>
                  {localeItem}
                  {/* {localeItem === 'cs-CZ' ? (
                      <FormattedMessage id="Footer.Language.Czech" defaultMessage={'Czech'} />
                    ) : (
                      <FormattedMessage id="Footer.Language.English" defaultMessage={'English'} />
                    )} */}
                </NextLink>
              </Text>
            ))}
        </Flex>
      </Container>
    </Box>
  );
};
