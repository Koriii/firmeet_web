import React from 'react';
import { Text } from '@chakra-ui/react';

interface IRoundedLabelProps {
  text: string;
}

export const RoundedLabel: React.FC<IRoundedLabelProps> = ({ text }) => {
  return (
    <Text
      textTransform="uppercase"
      bg={'green.100'}
      px={3}
      py={1}
      color={'gray.900'}
      fontSize="sm"
      fontWeight="600"
      rounded="xl">
      {text}
    </Text>
  );
};
