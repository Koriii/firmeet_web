import { useField } from 'formik';
import React, { ReactNode } from 'react';

interface ICheckboxFieldProps {
  children: ReactNode;
}

export const CheckboxField: React.FC<ICheckboxFieldProps> = ({ children, ...props }) => {
  const [field, meta] = useField({ ...props, type: 'checkbox', name: 'name' });

  return (
    <div>
      <label className="checkbox-input">
        <input type="checkbox" {...field} {...props} />
        {children}
      </label>
      {meta.touched && meta.error ? <div className="error">{meta.error}</div> : null}
    </div>
  );
};
