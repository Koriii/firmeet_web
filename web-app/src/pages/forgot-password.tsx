import React, { useState } from 'react';
import { Text } from '@chakra-ui/react';
import { Form, Formik } from 'formik';
import { LayoutComponent } from '../common/components';
import { Button, InputField, Wrapper } from '../common/components/shared/atoms';
import { useForgotPasswordMutation } from '../generated/graphql';
import { withApollo } from '../utils/withApollo';
import { FormattedMessage, useIntl } from 'react-intl';
import { useRouter } from 'next/router';

const initFormValues = { email: '' };

const ForgotPassword: React.FC<{}> = ({}) => {
  const { locale } = useRouter();
  const [complete, setComplete] = useState(false);
  const [forgotPassword] = useForgotPasswordMutation();
  const intl = useIntl();

  return (
    <LayoutComponent>
      <Wrapper variant="small">
        <Formik
          initialValues={initFormValues}
          onSubmit={async (values) => {
            await forgotPassword({ variables: { ...values, language: locale ?? 'en-US' } });
            setComplete(true);
          }}>
          {({ isSubmitting }) =>
            complete ? (
              <Text>
                <FormattedMessage id="ForgotPassword.LinkSent" defaultMessage="We sent the link to your email" />
              </Text>
            ) : (
              <Form>
                <InputField
                  name="email"
                  placeholder="email"
                  label={intl.formatMessage({ id: 'General.Email', defaultMessage: 'Email' })}
                  type="email"
                />

                <Button
                  mt={4}
                  type="submit"
                  isLoading={isSubmitting}
                  title={intl.formatMessage({ id: 'ForgotPassword.SendLinkButton', defaultMessage: 'Send link' })}
                />
              </Form>
            )
          }
        </Formik>
      </Wrapper>
    </LayoutComponent>
  );
};

export default withApollo({ ssr: false })(ForgotPassword);
