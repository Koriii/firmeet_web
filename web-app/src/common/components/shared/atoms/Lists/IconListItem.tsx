import { ListIcon } from '@chakra-ui/react';
import React from 'react';
import { FaCheckCircle, FaTimes } from 'react-icons/fa';
import { IconType } from '../../../../types/enums';

interface IIconListItem {
  iconType?: IconType;
  text: string;
}

export const IconListItem: React.FC<IIconListItem> = ({ text, iconType }) => {
  let icon;
  switch (iconType) {
    case IconType.CHECK: {
      icon = <ListIcon as={FaCheckCircle} color="green.500" />;
      break;
    }
    case IconType.TIMES: {
      icon = <ListIcon as={FaTimes} color="red.500" />;
      break;
    }
  }
  return (
    <>
      {iconType && <>{icon}</>}
      {text}
    </>
  );
};
