import { cloneDeep } from '@apollo/client/utilities';
import { Box, Input, Text } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';

import { Dropdown, SidemenuContainer } from '../../../common/components/shared/atoms';
import { CategoryTree } from '../../../common/components/shared/organisms';
import { useDebounce } from '../../../common/hooks/useDebounce';
import { countryOptions } from '../../../common/types/countries';
import { useCategoriesQuery } from '../../../generated/graphql';

import { ICategoryTreeElement } from '../models';
import { findCategoryElement, handleCategoryExpand } from '../utils';
import { processSearchedValues } from './utils';

interface IFilterValue {
  categoryId: number | null;
  countryCode: string;
}
interface IPostsFilterProps {
  filters: IFilterValue;
  setFilters: (filters: IFilterValue) => void;
}

export const PostsFilter: React.FC<IPostsFilterProps> = ({ filters, setFilters }) => {
  const intl = useIntl();

  const [stateCategories, setStateCategories] = useState<ICategoryTreeElement[]>([]);
  const [searchValue, setSearchValue] = useState('');
  const debouncedSearchValue: string = useDebounce<string>(searchValue, 300);

  const { data: categoryData } = useCategoriesQuery();

  useEffect(() => {
    if (categoryData) {
      setStateCategories(cloneDeep(categoryData.categories) as ICategoryTreeElement[]);
    }
  }, [categoryData]);

  useEffect(() => {
    const clear = !debouncedSearchValue;
    // TODO: fix this, cloneDeep should not me needed
    setStateCategories((prev) => processSearchedValues(cloneDeep(prev), debouncedSearchValue, clear));
  }, [debouncedSearchValue]);

  const selectCategoryCallback = (element: ICategoryTreeElement, expand: boolean) => {
    if (expand) {
      const clonedCategories = cloneDeep(stateCategories);
      const selectedElement = findCategoryElement(clonedCategories, element.id);
      if (selectedElement) {
        selectedElement.isExpanded = !selectedElement.isExpanded;
      }
      handleCategoryExpand(clonedCategories, element.id);
      setStateCategories(clonedCategories);
    } else {
      const tmpCategoryId = filters.categoryId !== element.id ? element.id : null;
      setFilters({ ...filters, categoryId: tmpCategoryId });
    }
  };

  return (
    <SidemenuContainer>
      <Text fontSize="x-large" fontWeight="bold">
        <FormattedMessage id="Filter.Title" defaultMessage="Filters" />
      </Text>

      <Box mt={4}>
        <Text fontSize="x-medium" fontWeight="bold">
          <FormattedMessage id="Filter.Category" defaultMessage="Categories" />
        </Text>
        <Input
          size="sm"
          placeholder={intl.formatMessage({ id: 'Filter.SearchPlaceholder', defaultMessage: 'Search...' })}
          onChange={(e) => setSearchValue(e.target.value)}
          value={searchValue}
        />
        <CategoryTree
          onClick={(element, expand) => selectCategoryCallback(element, expand)}
          isWrapped={false}
          categories={stateCategories}
          selectedCategoryId={filters.categoryId}
        />
      </Box>
      <Box mt={4}>
        <Text fontSize="x-medium" fontWeight="bold">
          <FormattedMessage id="General.CountryLabel" defaultMessage="Country" />
        </Text>
        <Dropdown
          name="countryCode"
          label={intl.formatMessage({ id: 'General.CountryLabel', defaultMessage: 'Country' })}
          items={countryOptions}
          onChange={(e) => setFilters({ ...filters, countryCode: e.target.value })}
          value={filters.countryCode}
        />
      </Box>
    </SidemenuContainer>
  );
};
