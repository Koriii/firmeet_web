import React from 'react';
import { Box } from '@chakra-ui/react';
import { Form, Formik, FormikErrors } from 'formik';
import { Button, InputField, Link, Wrapper } from '../common/components/shared/atoms';
import { MeDocument, MeQuery, useLoginMutation } from '../generated/graphql';
import { toErrorMap } from '../utils/toErrorMap';
import { useRouter } from 'next/dist/client/router';
import { LayoutComponent } from '../common/components';
import NextLink from 'next/link';
import { withApollo } from '../utils/withApollo';
import { FormattedMessage, useIntl } from 'react-intl';

interface ILoginValues {
  email: string;
  password: string;
}

const Login: React.FC = () => {
  const router = useRouter();
  const [login] = useLoginMutation();
  const intl = useIntl();

  const onSubmitCallback = async (values: ILoginValues, setErrors: (errors: FormikErrors<ILoginValues>) => void) => {
    const response = await login({
      variables: values,
      update: (cache, { data }) => {
        cache.writeQuery<MeQuery>({
          query: MeDocument,
          data: {
            __typename: 'Query',
            me: data?.login.user,
          },
        });
        cache.evict({ fieldName: 'demandPosts:{}' });
        cache.evict({ fieldName: 'supplyPosts:{}' });
        cache.gc();
      },
    });
    if (response.data?.login.errors) {
      setErrors(toErrorMap(response.data.login.errors));
    } else if (response.data?.login.user) {
      if (typeof router.query.next === 'string') {
        router.push(router.query.next);
      } else {
        router.push('/');
      }
    }
  };

  return (
    <LayoutComponent>
      <Wrapper variant="small">
        <Formik
          initialValues={{ email: '', password: '' }}
          onSubmit={(values, { setErrors }) => onSubmitCallback(values, setErrors)}>
          {({ isSubmitting }) => (
            <Form>
              <InputField
                name="email"
                placeholder="john@doe.com"
                label={intl.formatMessage({ id: 'General.Email', defaultMessage: 'Email' })}
              />
              <Box mt={4}>
                <InputField
                  name="password"
                  placeholder="password"
                  label={intl.formatMessage({ id: 'General.Password', defaultMessage: 'Password' })}
                  type="password"
                />
              </Box>

              <Box textAlign="right" mt={2}>
                <NextLink href="/forgot-password">
                  <Link size="sm">
                    <FormattedMessage id="Login.ForgotPassword" defaultMessage="Forgot password" />
                  </Link>
                </NextLink>
              </Box>
              <Button
                mt={4}
                type="submit"
                isLoading={isSubmitting}
                title={intl.formatMessage({ id: 'Login.LoginButton', defaultMessage: 'Login' })}
              />
            </Form>
          )}
        </Formik>
      </Wrapper>
    </LayoutComponent>
  );
};

export default withApollo({ ssr: false })(Login);
