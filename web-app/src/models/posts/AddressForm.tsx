import { Grid } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import { Switch } from '../../common/components/shared/atoms';
import SearchPlaceInputField from './SearchInput';
import { countryOptions } from '../../common/types/countries';
import { useIntl } from 'react-intl';
import { DropdownField } from '../../common/components/shared/atoms';

interface IAddressFormProps {
  setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void;
}

export interface IAddress {
  district: string;
  city: string;
  locality: string; // set for subLocality as well
  street: string;
  countryCode: string;
  streetNumber: string;
  postalCode: string;
}

const initAddress = {
  district: '',
  city: '',
  locality: '', // set for subLocality as well
  street: '',
  countryCode: 'CZ',
  streetNumber: '',
  postalCode: '',
};

export const AddressForm: React.FC<IAddressFormProps> = ({ setFieldValue }) => {
  const intl = useIntl();
  const [useCompanyAddress, setUseCompanyAddress] = useState(false);

  const [address, setAddress] = useState<IAddress>(initAddress);

  // triggers filter validation on screen load
  useEffect(() => {
    setFieldValue('useCompanyAddress', useCompanyAddress, false);
  }, [setFieldValue, useCompanyAddress]);

  return (
    <>
      <Switch
        name="useCompanyAddress"
        id="address-switch"
        title="Would you like to use company's address"
        onChange={() => {
          setUseCompanyAddress((prev) => !prev);
        }}
        isChecked={useCompanyAddress}
      />
      {!useCompanyAddress && (
        <>
          <Grid mt={2} gap="4" templateColumns={'repeat(2, 1fr)'}>
            <DropdownField
              name="countryCode"
              label={intl.formatMessage({ id: 'General.CountryLabel', defaultMessage: 'Country' })}
              items={countryOptions}
            />
            <SearchPlaceInputField
              componentId="district"
              placeholder={intl.formatMessage({
                id: 'Address.Form.DistrictPlaceholder',
                defaultMessage: 'district...',
              })}
              label={intl.formatMessage({ id: 'Address.Form.DistrictLabel', defaultMessage: 'District' })}
              value={address}
              setAddress={setAddress}
              setFieldValue={setFieldValue}
            />
            <SearchPlaceInputField
              componentId="city"
              placeholder={intl.formatMessage({ id: 'Address.Form.CityPlaceholder', defaultMessage: 'city...' })}
              label={intl.formatMessage({ id: 'Address.Form.CityLabel', defaultMessage: 'City' })}
              value={address}
              setAddress={setAddress}
              setFieldValue={setFieldValue}
            />
            <SearchPlaceInputField
              componentId="postalCode"
              placeholder={intl.formatMessage({
                id: 'Address.Form.PostCodePlaceholder',
                defaultMessage: 'postal code...',
              })}
              label={intl.formatMessage({ id: 'Address.Form.PostCodeLabel', defaultMessage: 'Postal code' })}
              value={address}
              setAddress={setAddress}
              setFieldValue={setFieldValue}
            />
            <SearchPlaceInputField
              componentId="street"
              placeholder={intl.formatMessage({ id: 'Address.Form.StreetPlaceholder', defaultMessage: 'street...' })}
              label={intl.formatMessage({ id: 'Address.Form.StreetLabel', defaultMessage: 'Street' })}
              value={address}
              setAddress={setAddress}
              setFieldValue={setFieldValue}
            />
            <SearchPlaceInputField
              componentId="streetNumber"
              placeholder={intl.formatMessage({ id: 'Address.Form.StreetNumberPlaceholder', defaultMessage: '24' })}
              label={intl.formatMessage({ id: 'Address.Form.StreetNumberLabel', defaultMessage: 'Street number' })}
              value={address}
              setAddress={setAddress}
              setFieldValue={setFieldValue}
            />
          </Grid>
        </>
      )}
    </>
  );
};
