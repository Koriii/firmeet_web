import { ICategoryTreeElement, ICreateDemandValues } from './models';

export const setValidations = (data: ICreateDemandValues) => {
  const errors = {} as any;

  for (const [key, value] of Object.entries(data)) {
    if (!value) {
      if (key === 'title') {
        errors[key] = `Field is required`;
      }
      if (key === 'text') {
        errors[key] = `Field is required`;
      }
      if (key === 'companyId') {
        errors[key] = `Field is required`;
      }
      if (key === 'categoryId') {
        errors[key] = `Field is required`;
      }
      if (!data.useCompanyAddress) {
        if (key === 'locality') {
          errors[key] = `Field is required`;
        }
        if (key === 'district') {
          errors[key] = `Field is required`;
        }
        if (key === 'city') {
          errors[key] = `Field is required`;
        }
        if (key === 'postalCode') {
          errors[key] = `Field is required`;
        }
        if (key === 'street') {
          errors[key] = `Field is required`;
        }
        if (key === 'streetNumber') {
          errors[key] = `Field is required`;
        }
      }
    }
  }

  return errors;
};

export const handleCategoryExpand = (categories: ICategoryTreeElement[], targetCategoryId: number) => {
  const unexpand = (children: ICategoryTreeElement[]) => {
    for (const child of children) {
      child.isExpanded = false;
      if (child.children && child.children.length > 0) {
        unexpand(child.children);
      }
    }
  };

  const recFind = (children: ICategoryTreeElement[]) => {
    for (const child of children) {
      if (child.id === targetCategoryId) {
        if (!child.isExpanded && child.children && child.children.length > 0) {
          unexpand(child.children);
        }
      } else if (child.children && child.children.length > 0) {
        recFind(child.children);
      }
    }
  };

  for (const category of categories) {
    if (category.id === targetCategoryId) {
      if (!category.isExpanded && category.children && category.children.length > 0) {
        unexpand(category.children);
      }
    } else if (category.children && category.children.length > 0) {
      recFind(category.children);
    }
  }
  return;
};

export const findCategoryElement = (categories: ICategoryTreeElement[], targetCategoryId: number) => {
  const recFind = (children: ICategoryTreeElement[]): ICategoryTreeElement | undefined => {
    for (const child of children) {
      if (child.id === targetCategoryId) {
        return child;
      } else if (child.children && child.children.length > 0) {
        recFind(child.children);
      }
    }
    return;
  };

  for (const category of categories) {
    if (category.id === targetCategoryId) {
      return category;
    } else if (category.children && category.children.length > 0) {
      const foundItem = recFind(category.children);
      if (foundItem) {
        return foundItem;
      }
    }
  }
  return;
};

// TODO: create base for type containing id and children and the rest will be passed from reference
export const findAnythingById = (items: any[], targetCategoryId: number) => {
  let foundItem;

  const recFind = (children: any[]): any => {
    for (const child of children) {
      if (child.id === targetCategoryId) {
        foundItem = child;
      } else if (child.children && child.children.length > 0) {
        recFind(child.children);
      }
    }
    return;
  };

  for (const item of items) {
    if (item.id === targetCategoryId) {
      return item;
    } else if (item.children && item.children.length > 0) {
      recFind(item.children);
      if (foundItem) {
        return foundItem;
      }
    }
  }
  return;
};
