export interface ISelectOption<TValue> {
  value: TValue;
  label: string;
}
