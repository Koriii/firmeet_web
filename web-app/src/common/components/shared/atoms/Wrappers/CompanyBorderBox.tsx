import { Box } from '@chakra-ui/react';
import React from 'react';

export const CompanyBorderBox: React.FC<{ children: React.ReactNode }> = ({ ...props }) => {
  return (
    <Box mt={4} p="5" width={400} border="1px" borderColor="gray.300" borderRadius="lg">
      {props.children}
    </Box>
  );
};
