import { Box, Text } from '@chakra-ui/react';
import { Form, Formik } from 'formik';
import { NextPage } from 'next';
import { useRouter } from 'next/dist/client/router';
import React, { useState } from 'react';
import { LayoutComponent } from '../../common/components';
import { Button, InputField, Link, Wrapper } from '../../common/components/shared/atoms';
import { MeDocument, MeQuery, useChangePasswordMutation } from '../../generated/graphql';
import { toErrorMap } from '../../utils/toErrorMap';
import { withApollo } from '../../utils/withApollo';

const ChangePassword: NextPage = () => {
  const router = useRouter();
  const [changePassword] = useChangePasswordMutation();
  const [tokenError, setTokenError] = useState('');

  return (
    <LayoutComponent>
      <Wrapper variant="small">
        <Formik
          initialValues={{ newPassword: '' }}
          onSubmit={async (values, { setErrors }) => {
            const response = await changePassword({
              variables: {
                newPassword: values.newPassword,
                token: typeof router.query.token === 'string' ? router.query.token : '',
              },
              update: (cache, { data }) => {
                cache.writeQuery<MeQuery>({
                  query: MeDocument,
                  data: {
                    __typename: 'Query',
                    me: data?.changePassword.user,
                  },
                });
              },
            });
            if (response.data?.changePassword.errors) {
              const errorMap = toErrorMap(response.data.changePassword.errors);
              if ('token' in errorMap) {
                setTokenError(errorMap.token);
              }
              setErrors(errorMap);
            } else if (response.data?.changePassword.user) {
              // worked
              router.push('/');
            }
          }}>
          {({ isSubmitting }) => (
            <Form>
              <InputField name="newPassword" placeholder="new password" label="New password" type="password" />
              {tokenError ? (
                <Box>
                  <Text size="sm" color="red">
                    {tokenError}
                  </Text>
                  <Link href="/forgot-password">Click here to get a new one</Link>
                </Box>
              ) : null}
              <Button mt={4} type="submit" isLoading={isSubmitting} title="Login" />
            </Form>
          )}
        </Formik>
      </Wrapper>
    </LayoutComponent>
  );
};

export default withApollo({ ssr: false })(ChangePassword);
