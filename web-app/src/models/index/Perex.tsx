import React from 'react';
import { Box, Container, Heading, Stack, Text, Flex } from '@chakra-ui/react';
import { gray, mainColor } from '../../utils/constants';
import { Button } from '../../common/components/shared/atoms';
import { FormattedMessage, useIntl } from 'react-intl';
import Image from 'next/image';

import profilePic from '../../perex-xl.jpeg';

export const Perex: React.FC = () => {
  const intl = useIntl();

  return (
    <Container maxW={'container.2xl'}>
      <Flex flexWrap={'wrap'}>
        <Stack as={Box} textAlign={'left'} spacing={{ base: 8, md: 14 }} py={{ base: 20, xs: 12, md: 36 }} flex="1">
          <Heading fontWeight={600} fontSize={{ base: '2xl', sm: '4xl', md: '6xl' }} lineHeight={'110%'}>
            <FormattedMessage id="Index.Perex.Title" defaultMessage="Your next great business" />
            <br />
            <Text as={'span'} color={mainColor}>
              <FormattedMessage id="Index.Perex.Subtitle" defaultMessage="at fingertips" />
            </Text>
          </Heading>
          <Text color={gray}>
            <FormattedMessage
              id="Index.Perex.Body"
              defaultMessage="Do you have a unused factory space, or are you looking for a place to advertise. Meet your next business or a partner with ease. Browse our lists of demand offers or a list of supply offers and ease the search. Our newsletter feature will inform you of any offers that might interest you."
            />
          </Text>
          <Box>
            <Button
              px={6}
              title={intl.formatMessage({ id: 'Index.Perex.DemandButton', defaultMessage: 'Demand list' })}
              href={'/demand-list'}
              fixedWidth
              mr={4}
            />
            <Button
              px={6}
              title={intl.formatMessage({ id: 'Index.Perex.SupplyButton', defaultMessage: 'Supply list' })}
              href={'/supply-list'}
              fixedWidth
            />
          </Box>
        </Stack>
        <Box>
          <Flex alignContent={'center'} justifyContent={'center'}>
            <Image
              src={profilePic}
              alt="Work together image"
              // layout="raw"
              width={700}
              height={500}
              // width={700}
              // height={500}
              // blurDataURL="data:..." automatically provided
              placeholder="blur" // Optional blur-up while loading
            />
          </Flex>
        </Box>
      </Flex>
    </Container>
  );
};
