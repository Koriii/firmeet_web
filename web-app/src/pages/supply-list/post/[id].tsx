import { Flex, Heading, Text } from '@chakra-ui/layout';
import { Box, Grid } from '@chakra-ui/react';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useRouter } from 'next/router';
import React, { useEffect, useMemo, useState } from 'react';
import { FormattedMessage } from 'react-intl';
import { LayoutComponent } from '../../../common/components';
import { BodyContainer, Button, Tag } from '../../../common/components/shared/atoms';
import { PostCompanyCard } from '../../../common/components/shared/molecules/Card/PostCompanyCard';
import { SideAddressCard } from '../../../common/components/shared/molecules/Card/SideAddressCard';
import { useGetSupplyPostById } from '../../../common/hooks/useGetPostFromUrl';
import { ISelectOption } from '../../../common/types/types';
import { useCategoriesQuery, useDeleteSupplyPostMutation, useMeQuery } from '../../../generated/graphql';
import { NotFound } from '../../../models/NotFound';
import { findAnythingById } from '../../../models/posts/utils';
import { isServerSide } from '../../../utils/isServer';
import { withApollo } from '../../../utils/withApollo';

const SupplyPost: React.FC = () => {
  const router = useRouter();
  const formattedId = useMemo<number>(() => Number(router.query.id), [router]);
  const [category, setCategory] = useState<ISelectOption<number> | null>(null);

  const { data: meData } = useMeQuery({
    skip: isServerSide(),
  });
  const { data: supplyData, loading } = useGetSupplyPostById(formattedId, meData?.me?.id);
  const { data: categoryData } = useCategoriesQuery();
  const [deletePost] = useDeleteSupplyPostMutation();

  useEffect(() => {
    if (categoryData && supplyData?.supplyPost) {
      const foundCategory = findAnythingById(categoryData.categories, supplyData.supplyPost.categoryId);
      if (foundCategory) {
        setCategory({ label: foundCategory.name, value: foundCategory.id });
      }
    }
  }, [categoryData, supplyData]);

  const onDeletePostCallback = async (id?: number) => {
    if (id) {
      await deletePost({
        variables: { id },
        update: (cache) => {
          cache.evict({ id: 'Supply:' + id });
        },
      });
      router.push('/supply-list');
    }
  };

  let content;
  if (loading) {
    content = <BodyContainer>...loading</BodyContainer>;
  } else if (!supplyData?.supplyPost) {
    content = <NotFound />;
  } else {
    content = (
      <BodyContainer type="form">
        <Tag size="lg" mb="8px">
          {category?.label}
        </Tag>
        <Heading mb="8px">{supplyData.supplyPost.title}</Heading>

        <Grid templateColumns={['repeat(1, 1fr)', '1fr 250px']} gap={6}>
          <Box>
            <Text whiteSpace="pre-wrap">{supplyData.supplyPost.text}</Text>
          </Box>
          <SideAddressCard data={supplyData.supplyPost} isBlured={!meData?.me?.id} />
        </Grid>

        <Box mt="18px">
          <Text fontSize={'xl'} fontWeight={'700'} width={'100%'}>
            <FormattedMessage id="Supply.Detail.CompanyInformation" defaultMessage={'Company information'} />
          </Text>
          <PostCompanyCard data={supplyData.supplyPost.company} isBlured={!meData?.me?.id} />
        </Box>

        <Flex>
          <Button
            mr="auto"
            onClick={() => router.back()}
            icon={<FontAwesomeIcon icon={faArrowLeft} width={10} height={10} />}
            variant="outline"
            title="back"
            isForm
          />
          {meData?.me?.id === supplyData.supplyPost.creatorId && (
            <Button
              colorScheme="red"
              onClick={() => onDeletePostCallback(supplyData.supplyPost?.id)}
              title="Delete"
              isForm
            />
          )}
        </Flex>
      </BodyContainer>
    );
  }

  return <LayoutComponent>{content}</LayoutComponent>;
};

export default withApollo({ ssr: true })(SupplyPost);
