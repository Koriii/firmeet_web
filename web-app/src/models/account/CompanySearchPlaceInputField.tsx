import Script from 'react-load-script';
import React, { useRef } from 'react';
import { countryOptions } from '../../common/types/countries';
import { InputField } from '../../common/components/shared/atoms';
import { GOOGLE_API_KEY } from '../../utils/constants';
import { CompanyInputModel } from '../../generated/graphql';

interface ISearchPlaceInputField {
  componentId: string;
  placeholder?: string;
  label?: string;
  value: CompanyInputModel;
  setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void;
}

const CompanySearchPlaceInputField: React.FC<ISearchPlaceInputField> = ({
  componentId,
  value,
  setFieldValue,
  label,
  placeholder,
}) => {
  // Store autocomplete object in a ref.
  // This is done because refs do not trigger a re-render when changed.
  const autocompleteRef = useRef<any>(null);

  const handleScriptLoad = () => {
    // Declare Options For Autocomplete
    const options = {
      types: ['address'],
      componentRestrictions: { country: [`${value.countryCode}`] },
    };
    const element = document.getElementById(componentId) as HTMLInputElement;

    if (element) {
      // Initialize Google Autocomplete
      autocompleteRef.current = new google.maps.places.Autocomplete(element, options);

      // Avoid paying for data that you don't need by restricting the set of
      // place fields that are returned to just the address components and formatted
      // address.
      autocompleteRef.current.setFields(['address_components', 'formatted_address']);

      // Fire Event when a suggested name is selected
      autocompleteRef.current.addListener('place_changed', handlePlaceSelect);
      console.log(autocompleteRef.current);
    }
  };

  const handlePlaceSelect = () => {
    console.log('handle place select');
    // Extract City From Address Object
    const addressObject = autocompleteRef.current.getPlace();
    const addressComponents = addressObject.address_components;

    // Check if address is valid
    if (addressComponents) {
      for (const component of addressComponents) {
        const type = component.types[0];

        if (type === 'locality') {
          value.locality = component.long_name;
          setFieldValue('locality', component.long_name);
        }
        if (type === 'street_number') {
          value.streetNumber = component.long_name;
          setFieldValue('streetNumber', component.long_name);
        }
        if (type === 'route') {
          value.street = component.long_name;
          setFieldValue('street', component.long_name);
        }
        if (type === 'sublocality_level_1') {
          value.locality = component.long_name;
          setFieldValue('locality', component.long_name);
        }
        if (type === 'postal_code') {
          value.postalCode = component.long_name;
          setFieldValue('postalCode', component.long_name);
        }
        if (type === 'country') {
          const country = countryOptions.find((countryOption) => countryOption.label === component.long_name);
          value.countryCode = country?.value ?? '';
          setFieldValue('countryCode', country?.value);
        }
        if (type === 'administrative_area_level_1') {
          value.district = component.long_name;
          setFieldValue('district', component.long_name);
        }
        if (type === 'administrative_area_level_2') {
          value.city = component.long_name;
          setFieldValue('city', component.long_name);
        }
      }
    }
  };

  // TODO: fix this ugly thing
  const tmpValue = value as any;

  return (
    <div>
      <Script
        url={`https://maps.googleapis.com/maps/api/js?key=${GOOGLE_API_KEY}&libraries=places`}
        onLoad={handleScriptLoad}
      />
      <InputField
        id={componentId}
        name={componentId}
        placeholder={placeholder}
        label={label}
        value={tmpValue[componentId]}
        onChange={(e) => setFieldValue(componentId, e.target.value)}
      />
    </div>
  );
};

export default CompanySearchPlaceInputField;
