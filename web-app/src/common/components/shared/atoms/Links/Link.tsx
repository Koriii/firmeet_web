// eslint-disable-next-line import/named
import { Link, LinkProps } from '@chakra-ui/react';
import React, { forwardRef, ForwardRefRenderFunction, Ref } from 'react';
import NextLink from 'next/link';

import { colorScheme } from '../../../../../utils/constants';

interface ILinkComponentProps extends LinkProps {
  size?: 'sm' | 'md' | 'lg';
  variant?: string;
  href?: string;
  linkAs?: string;
}

// eslint-disable-next-line react/prop-types
const InnerLinkComponent: ForwardRefRenderFunction<Ref<HTMLAnchorElement> | undefined, ILinkComponentProps> = ({
  size = 'sm',
  href,
  linkAs,
  ...rest
}) =>
  // ref,
  {
    const link = (
      <Link p={2} fontSize={size} fontWeight={500} colorScheme={colorScheme} {...rest}>
        {rest.children}
      </Link>
    );

    return (
      <>
        {href ? (
          <NextLink href={href} as={linkAs}>
            {link}
          </NextLink>
        ) : (
          <>{link}</>
        )}
      </>
    );
  };

export const LinkComponent = forwardRef(InnerLinkComponent);
