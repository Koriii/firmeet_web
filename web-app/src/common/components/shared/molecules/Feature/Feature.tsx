import React from 'react';
import { Stack, Flex, Text } from '@chakra-ui/react';

import { mainColor } from '../../../../../utils/constants';

interface IFeatureProps {
  title: string;
  content: string;
  icon: React.ReactElement;
  hasBorder?: boolean;
}

export const Feature: React.FC<IFeatureProps> = ({ title, content, icon }) => {
  return (
    <Stack align="center">
      <Flex w={16} h={16} align={'center'} justify={'center'} color={'white'} rounded={'full'} bg={mainColor} mb={1}>
        {icon}
      </Flex>
      <Text fontWeight={600}>{title}</Text>
      <Text align="center" color={'gray.600'}>
        {content}
      </Text>
    </Stack>
  );
};
