import { Box, Heading } from '@chakra-ui/layout';
import React, { useEffect, useState } from 'react';
import { FaPlusCircle, FaSave } from 'react-icons/fa';
import { FormattedMessage, useIntl } from 'react-intl';
import { LayoutComponent } from '../../common/components';
import { BodyContainer, Button, SidemenuContainer } from '../../common/components/shared/atoms';
import { CenteredSpinner, SideMenuItem } from '../../common/components/shared/molecules';
import { NotFound } from '../../models/NotFound';
import { CompanyInputModel, useCompaniesQuery, useMeQuery } from '../../generated/graphql';
import { CompanyForm } from '../../models/account/CompanyForm';
import { isServerSide } from '../../utils/isServer';
import { withApollo } from '../../utils/withApollo';
import { Flex, useToast } from '@chakra-ui/react';
import { useIsAuth } from '../../common/hooks/useIsAuth';
import { defaultToastConfig } from '../../utils/constants';

const initCompanyInput = {
  id: 0,
  name: '',
  cin: '',
  vat: '',
  street: '',
  city: '',
  district: '',
  postalCode: '',
  locality: '',
  streetNumber: '',
  countryCode: 'CZ',
  website: '',
};

// const menuItemList = ['Account', 'Company'];

const Account: React.FC = () => {
  useIsAuth();
  const intl = useIntl();
  const toast = useToast();

  // const [activeMenuItem, setActiveMenuItem] = useState<string>('Account');

  const { data, loading } = useMeQuery({
    skip: isServerSide(),
  });
  const { data: companyQueryData, loading: companyLoading } = useCompaniesQuery({
    variables: { creatorId: data?.me?.id ?? 0 },
  });

  const [companyData, setCompanyData] = useState<CompanyInputModel[]>([]);
  const [expandedId, setExpandedId] = useState<number | null>(null);

  const addCompanyCallback = () => {
    if (!companyData.some((company) => !company.id)) {
      setCompanyData((prev) => [...prev, { ...initCompanyInput }]);
      setExpandedId(0);
    } else {
      toast({
        ...defaultToastConfig,
        title: 'Warning',
        description: intl.formatMessage({
          id: 'Account.CreateWarning',
          defaultMessage: 'You need to complete the previous company first',
        }),
        status: 'warning',
      });
    }
  };

  useEffect(() => {
    if (companyQueryData) {
      const result = companyQueryData.companies.map(
        (company) =>
          ({
            ...company,
            id: company.id ?? 0,
          } as CompanyInputModel),
      );
      setCompanyData(result);
    }
  }, [companyQueryData]);

  const deleteCallback = (id: number) => {
    const res = companyData.filter((data) => data.id !== id);
    setCompanyData(res);
  };

  let content;
  if (loading) {
    content = <CenteredSpinner />;
  } else if (!data?.me?.id) {
    content = <NotFound />;
  } else {
    content = (
      <Flex>
        {/* <SidemenuContainer>
          <Box marginY={8} marginX={4}>
            {menuItemList.map((item) => (
              <SideMenuItem
                key={item}
                text={item}
                isActive={activeMenuItem === item}
                onClick={(name) => setActiveMenuItem(name)}
              />
            ))}
          </Box>
        </SidemenuContainer> */}
        {/* {
          {
            Account: (
              <BodyContainer textAlign="left" type="form">
                <Flex direction={'column'}>
                  <Heading as="h2">
                    <FormattedMessage id="Account.Title" defaultMessage="Account info" /> {data.me.email}
                  </Heading>
                </Flex>
              </BodyContainer>
            ),
            Company: ( */}
        <BodyContainer textAlign="left" type="form">
          <Flex align="center">
            <Heading as="h2">
              <FormattedMessage id="Account.Title" defaultMessage="Account info" /> {data.me.email}
            </Heading>
            <Button
              mb={4}
              ml="auto"
              onClick={addCompanyCallback}
              icon={<FaPlusCircle />}
              title={intl.formatMessage({ id: 'General.CreateButton', defaultMessage: 'Create' })}
            />
          </Flex>
          {!companyLoading &&
            companyData.map((company) => (
              <CompanyForm
                key={company.id}
                data={company}
                onDelete={deleteCallback}
                setExpandedId={setExpandedId}
                isExpanded={expandedId === company.id}
              />
            ))}
          {companyLoading && <CenteredSpinner />}
        </BodyContainer>
        {/* ),
          }[activeMenuItem]
        } */}
      </Flex>
    );
  }

  return <LayoutComponent>{content}</LayoutComponent>;
};

export default withApollo({ ssr: false })(Account);
