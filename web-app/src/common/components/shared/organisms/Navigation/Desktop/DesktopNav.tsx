import { useColorModeValue, Stack, Box, Popover, PopoverTrigger, PopoverContent } from '@chakra-ui/react';
import React from 'react';
import { Link } from '../../../atoms';
import { NAV_ITEMS } from '../constants';
import { DesktopSubNav } from './DesktopSubNav';

export const DesktopNav: React.FC = () => {
  const linkColor = useColorModeValue('gray.600', 'gray.200');
  const linkHoverColor = useColorModeValue('gray.800', 'white');
  const popoverContentBgColor = useColorModeValue('gray.100', 'gray.800');

  return (
    <Stack direction={'row'} spacing={4}>
      {NAV_ITEMS.map((navItem) => (
        <Box key={navItem.label}>
          <Popover trigger={'hover'} placement={'bottom-start'}>
            {/* <PopoverTrigger> */}
            <Link
              href={navItem.href ?? '#'}
              p={2}
              color={linkColor}
              _hover={{
                textDecoration: 'none',
                color: linkHoverColor,
                backgroundColor: popoverContentBgColor,
                borderRadius: '5px',
              }}>
              {navItem.label}
            </Link>
            {/* </PopoverTrigger> */}

            {navItem.children && (
              <PopoverContent border={0} boxShadow={'xl'} bg={popoverContentBgColor} p={4} rounded={'xl'} minW={'sm'}>
                <Stack>
                  {navItem.children.map((child) => (
                    <DesktopSubNav key={child.label} {...child} />
                  ))}
                </Stack>
              </PopoverContent>
            )}
          </Popover>
        </Box>
      ))}
    </Stack>
  );
};
