import { useField } from 'formik';
import { FormControl, FormErrorMessage, FormLabel } from '@chakra-ui/react';
import React, { SelectHTMLAttributes } from 'react';
import { ISelectOption } from '../../../../types/types';
import { Dropdown } from '..';

type IDropdownFieldProps = SelectHTMLAttributes<HTMLSelectElement> & {
  name: string;
  label?: string;
  items: ISelectOption<string | number>[];
  placeholder?: string;
  mt?: string;
};

export const DropdownField: React.FC<IDropdownFieldProps> = ({ items, label, mt, ...props }) => {
  const [field, { error }] = useField(props);

  return (
    <FormControl isInvalid={!!error} isRequired={props.required} mt={mt ?? 2}>
      {label && <FormLabel htmlFor={field.name}>{label}</FormLabel>}
      <Dropdown items={items} field={field} {...props} />
      {error ? <FormErrorMessage>{error}</FormErrorMessage> : null}
    </FormControl>
  );
};
