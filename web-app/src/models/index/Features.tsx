import { Box, SimpleGrid } from '@chakra-ui/react';
import { faAddressCard, faBandAid, faTruckFast } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { useIntl } from 'react-intl';
import { Feature } from '../../common/components/shared/molecules';

export const Features: React.FC = () => {
  const intl = useIntl();

  return (
    <Box p={4}>
      <SimpleGrid columns={{ base: 1, md: 3 }} spacing={10}>
        <Feature
          icon={<FontAwesomeIcon icon={faTruckFast} />}
          title={intl.formatMessage({
            id: 'Index.Features.FastReach.Title',
            defaultMessage: 'Reach your goals quick',
          })}
          content={intl.formatMessage({
            id: 'Index.Features.FastReach.Body',
            defaultMessage:
              'Quick and easy access to a wide variety of offers, have a contact, or post your offer wthin minutes for free.',
          })}
        />
        <Feature
          icon={<FontAwesomeIcon icon={faAddressCard} />}
          title={intl.formatMessage({
            id: 'Index.Features.Newsletter.Title',
            defaultMessage: 'Customizable newsletter',
          })}
          content={intl.formatMessage({
            id: 'Index.Features.Newsletter.Body',
            defaultMessage: 'Get notifications for your desired category by subscribing to our newsletter.',
          })}
        />
        <Feature
          icon={<FontAwesomeIcon icon={faBandAid} />}
          title={intl.formatMessage({
            id: 'Index.Features.EasyUsage.Title',
            defaultMessage: 'Easy to use',
          })}
          content={intl.formatMessage({
            id: 'Index.Features.EasyUsage.Body',
            defaultMessage: 'Add your companies in the profile overview and create your offers or demands with ease.',
          })}
        />
      </SimpleGrid>
    </Box>
  );
};
