import { HStack, Text } from '@chakra-ui/react';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { PropsWithChildren } from 'react';

interface ISideMenuItemProps {
  text: string;
  isActive?: boolean;
  onClick: (name: string) => void;
}

export const SideMenuItem: React.FC<PropsWithChildren<ISideMenuItemProps>> = ({ text, isActive, onClick }) => {
  return (
    <HStack cursor={'pointer'} onClick={() => onClick(text)} width={150} py={2}>
      <FontAwesomeIcon icon={faArrowRight} />
      <Text _hover={{ textDecoraton: 'underline' }} fontWeight={isActive ? 'bold' : ''}>
        {text}
      </Text>
    </HStack>
  );
};
