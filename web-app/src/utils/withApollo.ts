import { withApollo as createWithApollo } from 'next-apollo';
import { ApolloClient, InMemoryCache } from '@apollo/client';
import { PaginatedDemandPostsModel, PaginatedSupplyPostsModel } from '../generated/graphql';

const client = new ApolloClient({
  uri: process.env.NEXT_PUBLIC_API_URL as string,
  credentials: 'include',
  cache: new InMemoryCache({
    typePolicies: {
      Query: {
        fields: {
          demandPosts: {
            // The keyArgs list and merge function are the same as above.
            keyArgs: [], //'query' for searching for example, read docs
            merge(
              existing: PaginatedDemandPostsModel | undefined,
              incoming: PaginatedDemandPostsModel,
            ): PaginatedDemandPostsModel {
              return {
                ...incoming,
                posts: [...(existing?.posts || []), ...incoming.posts],
              };
            },
          },
          supplyPosts: {
            keyArgs: [], //'query' for searching for example, read docs
            merge(
              existing: PaginatedSupplyPostsModel | undefined,
              incoming: PaginatedSupplyPostsModel,
            ): PaginatedSupplyPostsModel {
              return {
                ...incoming,
                posts: [...(existing?.posts || []), ...incoming.posts],
              };
            },
          },
        },
      },
    },
  }),
});

export const withApollo = createWithApollo(client);
