import { Heading, Box, Text } from '@chakra-ui/react';
import React from 'react';
import { BorderBox, CardTags } from '../../atoms';
import NextLink from 'next/link';

interface ICardProps {
  id: number;
  title: string;
  tags: string[];
  href: string;
  as?: string;
  date: string;
  children: React.ReactNode;
}

export const PostCard: React.FC<ICardProps> = ({ title, tags, href, as, date, ...props }) => {
  const formattedDate = new Date(Number(date));

  return (
    <NextLink href={href} as={as}>
      <BorderBox>
        <Box
          // marginTop={{ base: '1', sm: '5' }}
          p={{ base: '2', sm: '5' }}
          display="flex"
          flexDirection={{ base: 'column', sm: 'row' }}
          justifyContent="space-between"
          pb={'4px'}
          _hover={{ cursor: 'pointer' }}>
          <Box
            display="flex"
            flex="1"
            flexDirection="column"
            justifyContent="center"
            marginTop={{ base: '3', sm: '0' }}>
            <CardTags tags={tags} />
            <Heading size="lg" marginTop="1">
              {title}
            </Heading>
            <Text as="p" marginTop="2" fontSize="lg" lineHeight="tight">
              {props.children}
            </Text>
          </Box>
        </Box>
        <Box position="absolute" bottom="5px" right="5px">
          <Text size="sm" color="gray.400" textAlign="right">
            {`${formattedDate.getUTCDate()}/${formattedDate.getUTCMonth()}/${formattedDate.getFullYear()}`}
          </Text>
        </Box>
      </BorderBox>
    </NextLink>
  );
};
