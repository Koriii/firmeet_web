import { Box, BoxProps } from '@chakra-ui/layout';
import React from 'react';

interface ISidemenuContainerProps extends BoxProps {
  children: React.ReactNode;
}

export const SidemenuContainer: React.FC<ISidemenuContainerProps> = ({ children, ...props }) => {
  return (
    <Box borderRight={{ base: '0', sm: '1px' }} p="4" height="100%" borderColor={'gray.300'} {...props}>
      {children}
    </Box>
  );
};
