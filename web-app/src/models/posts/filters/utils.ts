import { ICategoryTreeElement } from '../models';

const expandItems = (categories: ICategoryTreeElement[], clear: boolean) => {
  const focusedElementIds = [] as number[];

  const expandChildren = (children: ICategoryTreeElement[]) => {
    for (const child of children) {
      if (child.children && child.children.length > 0) {
        expandChildren(child.children);
        if (
          child.children.some((element) => element.isHighlighted) ||
          (child.children.some((element) => focusedElementIds.includes(element.id)) && !clear)
        ) {
          focusedElementIds.push(child.id);
          child.isExpanded = true;
        } else {
          child.isExpanded = false;
        }
      }
    }
  };

  for (const category of categories) {
    if (category.children && category.children.length > 0) {
      expandChildren(category.children);
      if (
        category.children.some((element) => element.isHighlighted) ||
        (category.children.some((element) => focusedElementIds.includes(element.id)) && !clear)
      ) {
        focusedElementIds.push(category.id);
        category.isExpanded = true;
      } else {
        category.isExpanded = false;
      }
    }
  }
};

export const processSearchedValues = (categories: ICategoryTreeElement[], value: string, clear: boolean) => {
  const searchChildren = (children: ICategoryTreeElement[]) => {
    for (const child of children) {
      if (child.name.toLocaleLowerCase().includes(value.toLocaleLowerCase()) && !clear) {
        child.isHighlighted = true;
      } else {
        child.isHighlighted = false;
      }
      if (child.children && child.children.length > 0) {
        searchChildren(child.children);
      }
    }
  };

  for (const category of categories) {
    if (category.name.toLocaleLowerCase().includes(value.toLocaleLowerCase()) && !clear) {
      category.isHighlighted = true;
    } else {
      category.isHighlighted = false;
    }
    if (category.children && category.children.length > 0) {
      searchChildren(category.children);
    }
  }
  expandItems(categories, clear);

  return categories;
};
