import { Center, HStack, Text } from '@chakra-ui/layout';
import React from 'react';
import { FaArrowDown, FaArrowRight } from 'react-icons/fa';
import { BorderBox } from '../../atoms';

interface ICategoryCardProps {
  name: string;
  isActive?: boolean;
  isExpanded?: boolean;
  hasChildren?: boolean;
  depthMargin: number;
  isWrapped?: boolean;
  isHighlighted?: boolean;
  onClick: (expand: boolean) => void;
}

export const CategoryCard: React.FC<ICategoryCardProps> = ({
  name,
  isActive = false,
  isExpanded = false,
  hasChildren = false,
  isWrapped = true,
  depthMargin,
  isHighlighted = false,
  onClick,
}) => {
  let computedMargin = 0;
  if (depthMargin === 1) {
    if (hasChildren) {
      computedMargin = 0;
    } else {
      computedMargin = 6;
    }
  } else {
    computedMargin = 6 * depthMargin;
  }

  return (
    <HStack mt={isWrapped ? 4 : 2} ml={computedMargin}>
      {hasChildren && (
        <Center display="inline-block" bottom="8" onClick={() => onClick(true)} cursor="pointer">
          {isExpanded ? <FaArrowDown /> : <FaArrowRight />}
        </Center>
      )}
      {isWrapped ? (
        <BorderBox
          py="1"
          px="2"
          cursor="pointer"
          minWidth={120}
          isActive={isActive}
          display="inline-block"
          bgColor={isHighlighted ? '#FAF089' : ''}
          borderRadius={'sm'}
          onClick={() => onClick(false)}>
          <Text textAlign="left">{name}</Text>
        </BorderBox>
      ) : (
        <Text
          px="2"
          textAlign="left"
          fontWeight={isActive ? '700' : ''}
          cursor="pointer"
          borderBottom={isActive ? '1px' : ''}
          bgColor={isHighlighted ? '#FAF089' : ''}
          borderRadius={'sm'}
          onClick={() => onClick(false)}>
          {name}
        </Text>
      )}
    </HStack>
  );
};
