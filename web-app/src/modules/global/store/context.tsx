import React, { useContext, useReducer } from 'react';
import { useActions } from './actions';
import { IGlobalContextValue, initGlobalContextValue, initGlobalState } from './models';

import { reducer } from './reducers';

export const GlobalContext = React.createContext<IGlobalContextValue>(initGlobalContextValue);

export const GlobalContextProvider: React.FC = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initGlobalState);
  const actions = useActions(dispatch);

  return <GlobalContext.Provider value={{ state, actions }}>{children}</GlobalContext.Provider>;
};

export const useGlobalContext = () => useContext(GlobalContext);
