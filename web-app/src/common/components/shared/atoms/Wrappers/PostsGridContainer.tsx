import { Grid } from '@chakra-ui/react';
import React from 'react';

export const PostsGridContainer: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  return (
    <Grid templateColumns={['repeat(1, 1fr)', '300px 1fr']} gap={6}>
      {children}
    </Grid>
  );
};
