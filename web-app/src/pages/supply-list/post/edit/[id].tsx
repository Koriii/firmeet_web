import { Heading, Text } from '@chakra-ui/layout';
// import { Flex, Spinner, Stack } from '@chakra-ui/react';
// import { Form, Formik } from 'formik';
// import { useRouter } from 'next/router';
// import React, { useCallback, useMemo } from 'react';
// import { LayoutComponent } from '../../../../common/components';

// import { BodyContainer, Button, Dropdown, InputField } from '../../../../common/components/shared/atoms';
// import { useGetSupplyPostById } from '../../../../common/hooks/useGetPostFromUrl';
// import { useIsAuth } from '../../../../common/hooks/useIsAuth';
// import {
//   useCompanyOptionsQuery,
//   useDeleteSupplyPostMutation,
//   useMeQuery,
//   useUpdateSupplyPostMutation,
// } from '../../../../generated/graphql';
// import { NotFound } from '../../../../models/NotFound';
// import { isServerSide } from '../../../../utils/isServer';
// import { withApollo } from '../../../../utils/withApollo';

// interface IUpdateFormValues {
//   title: string;
//   text: string;
//   companyId: number;
// }

// const SupplyEditPost: React.FC = ({}) => {
//   useIsAuth();
//   const router = useRouter();
//   const formattedId = useMemo<number>(() => Number(router.query.id), [router.query.id]);

//   const { data, loading } = useGetSupplyPostById(formattedId);
//   const { data: meData } = useMeQuery({
//     skip: isServerSide(),
//   });
//   const { data: companyOptions } = useCompanyOptionsQuery({ variables: { creatorId: meData?.me?.id ?? 0 } });

//   const [deletePost] = useDeleteSupplyPostMutation();
//   const [updatePost] = useUpdateSupplyPostMutation();

//   const onDeletePostCallback = useCallback(
//     async (id?: number) => {
//       if (id) {
//         await deletePost({
//           variables: { id },
//           update: (cache) => {
//             cache.evict({ id: 'Supply:' + id });
//           },
//         });
//         router.push('/supply-list');
//       }
//     },
//     [deletePost, router],
//   );

//   // no need to update cache because URQL updates the post object with new values from UpdatePost.graphql
//   const onSubmitPostCallback = useCallback(
//     async (values: IUpdateFormValues) => {
//       await updatePost({ variables: { id: formattedId, input: values } });
//       router.back();
//     },
//     [updatePost, router],
//   );

//   let content;
//   if (loading) {
//     content = (
//       <BodyContainer textAlign="center">
//         <Spinner size="xl" />
//       </BodyContainer>
//     );
//   } else if (!data?.supplyPost) {
//     content = (
//       <BodyContainer textAlign="center">
//         <NotFound />
//       </BodyContainer>
//     );
//   } else {
//     content = (
//       <BodyContainer>
//         <Formik
//           initialValues={{
//             title: data.supplyPost.title,
//             text: data.supplyPost.text,
//             companyId: data.supplyPost.companyId,
//           }}
//           onSubmit={(values) => onSubmitPostCallback(values)}>
//           <Form>
//             <InputField label="Title" name="title" />
//             <InputField label="Body" name="text" rows={16} textarea />
//             <Dropdown name="companyId" label="Company" items={companyOptions} />

//             <Flex>
//               <Stack direction="row" spacing={4} ml="auto" mt="4">
//                 <Button colorScheme="red" onClick={() => onDeletePostCallback(data.supplyPost?.id)} title="Delete" />
//                 <Button type="submit" title="Save" />
//               </Stack>
//             </Flex>
//           </Form>
//         </Formik>
//       </BodyContainer>
//     );
//   }

//   return <LayoutComponent>{content}</LayoutComponent>;
// };

// export default withApollo({ ssr: false })(SupplyEditPost);
