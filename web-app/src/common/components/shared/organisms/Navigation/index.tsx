import { useApolloClient } from '@apollo/client';
import React from 'react';
import { Box, Flex, Text, IconButton, Stack, Collapse, useBreakpointValue, useDisclosure } from '@chakra-ui/react';
import { HamburgerIcon, CloseIcon } from '@chakra-ui/icons';
import NextLink from 'next/link';
import Image from 'next/image';
import { useIntl } from 'react-intl';

import { MobileNav } from './Mobile/MobileNav';
import { DesktopNav } from './Desktop/DesktopNav';

import { Button, Link } from '../../atoms';
import { useLogoutMutation, useMeQuery } from '../../../../../generated/graphql';
import { isServerSide } from '../../../../../utils/isServer';
import { Loader } from '../../../../../models/Loader';
import pic from '../../../../../Firmeet.png';

export const HeaderComponent: React.FC = () => {
  const apolloClient = useApolloClient();
  const intl = useIntl();
  const { isOpen, onToggle } = useDisclosure();

  const [logout, { loading: logoutFetching }] = useLogoutMutation();
  const { data, loading } = useMeQuery({
    skip: isServerSide(),
  });

  let registerElement = null;
  if (loading) {
    // loading
    registerElement = <Loader />;
  } else if (!data?.me || !data.me.id) {
    registerElement = (
      <>
        <Button
          variant={'link'}
          href="/login"
          title={intl.formatMessage({ id: 'Nav.SignIn', defaultMessage: 'Sign in' })}
        />
        <Button
          display={{ base: 'none', md: 'inline-flex' }}
          href="/register"
          title={intl.formatMessage({ id: 'Nav.SignUp', defaultMessage: 'Sign up' })}
        />
      </>
    );
    // not logged in
  } else {
    // logged in
    registerElement = (
      <>
        <Link href={'/account/[id]'} linkAs={`/account/${data?.me.id}`}>
          {data?.me.email}
        </Link>
        <Button
          onClick={async () => {
            await logout();
            await apolloClient.resetStore();
          }}
          variant="link"
          isLoading={logoutFetching}
          title="Log out"
        />
      </>
    );
  }

  return (
    <Box>
      <Flex
        bg={'white'}
        color={'gray.600'}
        minH={'60px'}
        py={{ base: 2 }}
        px={{ base: 4 }}
        borderBottom={1}
        borderStyle={'solid'}
        borderColor={'gray.300'}
        align={'center'}>
        <Flex flex={{ base: 1, md: 'auto' }} ml={{ base: -2 }} display={{ base: 'flex', md: 'none' }}>
          <IconButton
            onClick={onToggle}
            icon={isOpen ? <CloseIcon w={3} h={3} /> : <HamburgerIcon w={5} h={5} />}
            variant={'ghost'}
            aria-label={'Toggle Navigation'}
          />
        </Flex>
        <Flex flex={{ base: 1 }} justify={{ base: 'center', md: 'start' }}>
          <NextLink href="/">
            <Text
              textAlign={useBreakpointValue({ base: 'center', md: 'left' })}
              fontFamily={'heading'}
              color={'gray.800'}
              cursor="pointer">
              <Image
                src={pic}
                alt="Work together image"
                width={131}
                height={28}
                // blurDataURL="data:..." automatically provided
                placeholder="blur" // Optional blur-up while loading
              />
            </Text>
          </NextLink>

          <Flex display={{ base: 'none', md: 'flex' }} ml={10}>
            <DesktopNav />
          </Flex>
        </Flex>

        <Stack flex={{ base: 1, md: 0 }} justify={'flex-end'} direction={'row'} spacing={6}>
          {registerElement}
        </Stack>
      </Flex>

      <Collapse in={isOpen} animateOpacity>
        <MobileNav />
      </Collapse>
    </Box>
  );
};
