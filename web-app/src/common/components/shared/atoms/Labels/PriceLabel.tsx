import { HStack, Text } from '@chakra-ui/layout';
import React from 'react';

interface IPriceLabelProps {
  price: string;
}

export const PriceLabel: React.FC<IPriceLabelProps> = ({ price }) => {
  return (
    <HStack justifyContent="center">
      <Text fontSize="3xl" fontWeight="600">
        $
      </Text>
      <Text fontSize="5xl" fontWeight="900">
        {price}
      </Text>
      <Text fontSize="3xl" color="gray.500">
        /month
      </Text>
    </HStack>
  );
};
