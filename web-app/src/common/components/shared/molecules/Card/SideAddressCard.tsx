import { Grid, Box, Text } from '@chakra-ui/react';
import React from 'react';
import { FormattedMessage } from 'react-intl';
import { Maybe } from '../../../../../generated/graphql';

interface ISideAddressCardData {
  __typename?: 'Demand' | 'Supply';
  id: number;
  title: string;
  text: string;
  creatorId: number;
  categoryId: number;
  companyId: number;
  countryCode: string;
  district: string;
  city: string;
  postalCode: string;
  street: string;
  streetNumber: string;
  updatedAt: string;
  createdAt: string;
  creator: { __typename?: 'User'; id: number; email: string };
  company: {
    __typename?: 'Company';
    name: string;
    cin?: Maybe<string>;
    vat?: Maybe<string>;
    countryCode: string;
    website?: Maybe<string>;
    district: string;
    city: string;
    postalCode: string;
    street: string;
    streetNumber: string;
    locality: string;
  };
}

const bluredObject = { color: 'transparent', textShadow: '0 0 8px rgba(0,0,0,0.8)' };

interface ISideAddressCardProps {
  data: ISideAddressCardData;
  isBlured?: boolean;
}

export const SideAddressCard: React.FC<ISideAddressCardProps> = ({ data, isBlured = false }) => {
  const blured = isBlured ? bluredObject : {};

  return (
    <>
      {data && (
        <Box borderLeft="1px" borderColor="gray.200" pl="8">
          <Text fontSize={'lg'} fontWeight={'700'}>
            <FormattedMessage id="Demand.Detail.Address" defaultMessage={'Address'} />
          </Text>
          <Text {...blured}>
            {data.countryCode}
            <br />
            {data.district}
            <br />
            {data.city}, {data.postalCode}
            <br />
            {data.street}, {data.streetNumber}
          </Text>
        </Box>
      )}
    </>
  );
};
