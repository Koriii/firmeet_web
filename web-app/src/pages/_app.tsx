import { ChakraProvider, ColorModeProvider } from '@chakra-ui/react';
import { useRouter } from 'next/router';
import React, { useMemo } from 'react';
import { IntlProvider } from 'react-intl';

import Czech from '../compiled-locales/cs.json';
import English from '../compiled-locales/en.json';

import theme from '../theme';

const MyApp: React.FC = ({ Component, pageProps }: any) => {
  const { locale } = useRouter();
  const [shortLocale] = locale ? locale.split('-') : ['en'];

  const messages = useMemo(() => {
    switch (shortLocale) {
      case 'cs':
        return Czech;
      case 'en':
        return English;
      default:
        return English;
    }
  }, [shortLocale]);

  return (
    <ChakraProvider resetCSS theme={theme}>
      <ColorModeProvider
        value="light"
        options={{
          useSystemColorMode: false,
        }}>
        <IntlProvider key={locale} locale={shortLocale} messages={messages} onError={() => null}>
          <Component {...pageProps} />
        </IntlProvider>
      </ColorModeProvider>
    </ChakraProvider>
  );
};

export default MyApp;
