import Script from 'react-load-script';
import React, { useRef } from 'react';
import { InputField } from '../../common/components/shared/atoms';
import { GOOGLE_API_KEY } from '../../utils/constants';
import { IAddress } from './AddressForm';
import { countryOptions } from '../../common/types/countries';

interface ISearchPlaceInputField {
  componentId: string;
  placeholder?: string;
  label?: string;
  value: IAddress;
  setAddress: React.Dispatch<React.SetStateAction<IAddress>>;
  setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void;
}

const SearchPlaceInputField: React.FC<ISearchPlaceInputField> = ({
  componentId,
  placeholder,
  label,
  value,
  setAddress,
  setFieldValue,
}) => {
  // Store autocomplete object in a ref.
  // This is done because refs do not trigger a re-render when changed.
  const autocompleteRef = useRef<any>(null);

  const handleScriptLoad = () => {
    // Declare Options For Autocomplete
    const options = {
      // types: ['(regions)'],
      types: ['address'],
      componentRestrictions: { country: [`${value.countryCode}`] },
    };
    const element = document.getElementById(componentId) as HTMLInputElement;

    if (element) {
      // Initialize Google Autocomplete
      autocompleteRef.current = new google.maps.places.Autocomplete(element, options);

      // Avoid paying for data that you don't need by restricting the set of
      // place fields that are returned to just the address components and formatted
      // address.
      autocompleteRef.current.setFields(['address_components', 'formatted_address']);

      // Fire Event when a suggested name is selected
      autocompleteRef.current.addListener('place_changed', handlePlaceSelect);
    }
  };

  const handlePlaceSelect = () => {
    // Extract City From Address Object
    const addressObject = autocompleteRef.current.getPlace();
    const addressComponents = addressObject.address_components;

    // Check if address is valid
    if (addressComponents) {
      setAddress((prev) => {
        const result = { ...prev } as IAddress;

        for (const component of addressComponents) {
          if (component.types[0] === 'locality') {
            result.locality = component.long_name;
            setFieldValue('locality', component.long_name);
          }
          if (component.types[0] === 'street_number') {
            result.streetNumber = component.long_name;
            setFieldValue('streetNumber', component.long_name);
          }
          if (component.types[0] === 'route') {
            result.street = component.long_name;
            setFieldValue('street', component.long_name);
          }
          if (component.types[0] === 'sublocality_level_1') {
            result.locality = component.long_name;
            setFieldValue('locality', component.long_name);
          }
          if (component.types[0] === 'postal_code') {
            result.postalCode = component.long_name;
            setFieldValue('postalCode', component.long_name);
          }
          if (component.types[0] === 'country') {
            const country = countryOptions.find((countryOption) => countryOption.label === component.long_name);
            result.countryCode = country?.value ?? '';
            setFieldValue('countryCode', country?.value);
          }
          if (component.types[0] === 'administrative_area_level_1') {
            result.district = component.long_name;
            setFieldValue('district', component.long_name);
          }
          if (component.types[0] === 'administrative_area_level_2') {
            result.city = component.long_name;
            setFieldValue('city', component.long_name);
          }
        }
        return result;
      });
    }
  };

  // TODO: fix this ugly thing
  const tmpValue = value as any;

  return (
    <div>
      <Script
        url={`https://maps.googleapis.com/maps/api/js?key=${GOOGLE_API_KEY}&libraries=places`}
        onLoad={handleScriptLoad}
      />
      <InputField
        id={componentId}
        name={componentId}
        placeholder={placeholder}
        label={label}
        value={tmpValue[componentId]}
        required
        onChange={(e) => {
          setAddress((prev) => ({ ...prev, [componentId]: e.target.value } as IAddress));
          setFieldValue(componentId, e.target.value);
        }}
      />
    </div>
  );
};

export default SearchPlaceInputField;
