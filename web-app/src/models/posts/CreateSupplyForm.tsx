import { cloneDeep } from '@apollo/client/utilities';
import { Box, Flex } from '@chakra-ui/layout';
import { FormControl, FormErrorMessage, FormLabel, Input } from '@chakra-ui/react';
import { Form, Formik } from 'formik';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { FormattedMessage, useIntl } from 'react-intl';
import { InputField, Button, Link } from '../../common/components/shared/atoms';
import { DropdownField } from '../../common/components/shared/atoms';
import { CategoryTree } from '../../common/components/shared/organisms';
import { useDebounce } from '../../common/hooks/useDebounce';
import {
  useMeQuery,
  useCreateSupplyPostMutation,
  useCategoriesQuery,
  useCompanyOptionsQuery,
} from '../../generated/graphql';
import { isServerSide } from '../../utils/isServer';
import { AddressForm } from './AddressForm';
import { processSearchedValues } from './filters/utils';
import { ICategoryTreeElement, ICreateSupplyValues, initCreateSupplyValues } from './models';
import { findCategoryElement, handleCategoryExpand, setValidations } from './utils';

export const CreateSupplyForm: React.FC = () => {
  const intl = useIntl();
  const router = useRouter();
  const { data: meData } = useMeQuery({
    skip: isServerSide(),
  });
  const [createPost] = useCreateSupplyPostMutation();

  const [searchValue, setSearchValue] = useState('');
  const debouncedSearchValue: string = useDebounce<string>(searchValue, 300);

  const { data: categoryData } = useCategoriesQuery();
  const { data: companyOptions } = useCompanyOptionsQuery({ variables: { creatorId: meData?.me?.id ?? 0 } });

  const [selectedCategoryId, setSelectedCategoryId] = useState<number | null>(null);
  const [stateCategories, setStateCategories] = useState<ICategoryTreeElement[]>([]);

  const onSubmitCallback = async (values: ICreateSupplyValues) => {
    const { errors } = await createPost({
      variables: { input: { ...values, companyId: Number(values.companyId) } },
      update: (cache) => {
        cache.evict({ fieldName: 'supplyPosts:{}' }); // can be fieldName: 'posts'
      },
    });
    if (!errors) {
      router.push('/supply-list');
    }
  };

  useEffect(() => {
    if (categoryData) {
      setStateCategories(categoryData.categories as ICategoryTreeElement[]);
    }
  }, [categoryData, setStateCategories]);

  useEffect(() => {
    const clear = debouncedSearchValue ? false : true;
    // TODO: fix this, cloneDeep should not me needed
    setStateCategories((prev) => processSearchedValues(cloneDeep(prev), debouncedSearchValue, clear));
  }, [debouncedSearchValue]);

  const selectCallback = (element: ICategoryTreeElement, expand: boolean) => {
    if (expand) {
      const clonedCategories = cloneDeep(stateCategories) as ICategoryTreeElement[];
      const selectedElement = findCategoryElement(clonedCategories, element.id);
      if (selectedElement) {
        selectedElement.isExpanded = !selectedElement.isExpanded;
      }
      handleCategoryExpand(clonedCategories, element.id);
      setStateCategories(clonedCategories);
    } else {
      setSelectedCategoryId((prev) => (prev !== element.id ? element.id : null));
    }
  };

  return (
    <Formik
      initialValues={initCreateSupplyValues}
      onSubmit={onSubmitCallback}
      validate={(data) => setValidations(data)}>
      {({ isSubmitting, errors, setFieldValue }) => (
        <Form>
          <>
            <InputField
              name="title"
              placeholder={intl.formatMessage({ id: 'Supply.Form.TitlePlaceholder', defaultMessage: 'Title' })}
              label={intl.formatMessage({ id: 'Supply.Form.Title', defaultMessage: 'Title' })}
              required
            />
            <InputField
              textarea
              name="text"
              placeholder={intl.formatMessage({ id: 'Supply.Form.ContentPlaceholder', defaultMessage: 'Text...' })}
              label={intl.formatMessage({ id: 'Supply.Form.Body', defaultMessage: 'Content' })}
              required
            />

            <FormControl mt={2} isInvalid={companyOptions?.companyOptions.length === 0}>
              <Box>
                <DropdownField
                  name="companyId"
                  label={intl.formatMessage({ id: 'Supply.Form.Company', defaultMessage: 'Company' })}
                  items={companyOptions?.companyOptions ?? []}
                />
              </Box>
              <FormErrorMessage>
                <FormattedMessage id="Supply.Form.CompanyInfo" defaultMessage="Company can be added in a" />
                <Link href={'/account/[id]'} linkAs={`/account/${meData?.me?.id}`} textDecoration={'underline'}>
                  <FormattedMessage id="Supply.Form.CompanyInfoLink" defaultMessage="profile overview" />
                </Link>
              </FormErrorMessage>
            </FormControl>

            <AddressForm setFieldValue={setFieldValue} />

            <FormControl mt={2} mb={2} isInvalid={!!errors.categoryId} isRequired={true}>
              <FormLabel>
                <FormattedMessage id="Supply.Form.Category" defaultMessage="Category" />
              </FormLabel>

              <Input
                size="sm"
                onChange={(e) => setSearchValue(e.target.value)}
                value={searchValue}
                width={300}
                isRequired={false}
              />

              <CategoryTree
                onClick={(element, expand) => {
                  selectCallback(element, expand);
                  if (!expand) {
                    setFieldValue('categoryId', element.id);
                  }
                }}
                categories={stateCategories}
                selectedCategoryId={selectedCategoryId}
              />
              <FormErrorMessage>{errors.categoryId}</FormErrorMessage>
            </FormControl>

            <Flex>
              <Button
                ml="auto"
                mt={4}
                type="submit"
                isLoading={isSubmitting}
                title={intl.formatMessage({ id: 'Supply.Form.CreateButton', defaultMessage: 'Create supply' })}
              />
            </Flex>
          </>
        </Form>
      )}
    </Formik>
  );
};
