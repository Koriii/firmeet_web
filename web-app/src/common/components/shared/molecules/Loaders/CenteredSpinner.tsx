import React from 'react';
import { Loader } from '../../../../../models/Loader';
import { BodyContainer } from '../../atoms';

export const CenteredSpinner: React.FC = () => {
  return (
    <BodyContainer textAlign="center">
      <Loader />
    </BodyContainer>
  );
};
