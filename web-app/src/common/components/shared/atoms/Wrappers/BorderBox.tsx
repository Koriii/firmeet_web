import { Box, BoxProps } from '@chakra-ui/react';
import React from 'react';

interface IBorderBoxProps extends BoxProps {
  isActive?: boolean;
}

export const BorderBox: React.FC<IBorderBoxProps> = ({ isActive, ...props }) => {
  return (
    <Box
      position="relative"
      borderWidth="1px"
      borderRadius="lg"
      _hover={{ borderColor: 'gray.700' }}
      borderColor={isActive ? 'green.400' : ''}
      overflow="hidden"
      {...props}>
      {props.children}
    </Box>
  );
};
