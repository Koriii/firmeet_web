import { Dispatch, useCallback } from 'react';
import { GlobalAction, GlobalActionList, IGlobalActions } from './models';

export const useActions = (dispatch: Dispatch<GlobalAction>): IGlobalActions => {
  const setFilterValue = useCallback(
    (categoryId: number, type: any) => {
      dispatch({ type: GlobalActionList.SET_DEMAND_FILTER, payload: categoryId });
      dispatch({ type: GlobalActionList.SET_SUPPLY_FILTER, payload: categoryId });
    },
    [dispatch],
  );

  return { setFilterValue };
};
