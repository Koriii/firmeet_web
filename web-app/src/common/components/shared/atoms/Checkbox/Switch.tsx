import { FormControl, FormLabel, Switch, SwitchProps } from '@chakra-ui/react';
import React from 'react';

interface ISwitchProps extends SwitchProps {
  title: string;
  id: string;
}

export const SwitchComponent: React.FC<ISwitchProps> = ({ title, id, ...props }) => {
  return (
    <FormControl display="flex" alignItems="center" mt="2">
      <Switch id={id} colorScheme="green" {...props} />
      <FormLabel htmlFor={id} mb="0" ml="2">
        {title}
      </FormLabel>
    </FormControl>
  );
};
