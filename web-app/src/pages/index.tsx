import React from 'react';
import { LayoutComponent } from '../common/components';
import { BodyContainer } from '../common/components/shared/atoms';
import { Newsletter } from '../models/index/Newsletter';
import { Features, Perex } from '../models/index';
import { withApollo } from '../utils/withApollo';

const Index: React.FC = () => {
  return (
    <>
      <LayoutComponent>
        <BodyContainer>
          <Perex />
        </BodyContainer>
        <BodyContainer bgColor="gray.100" maxW="100%" m="0">
          <Features />
        </BodyContainer>
        <BodyContainer>
          <Newsletter />
        </BodyContainer>
      </LayoutComponent>
    </>
  );
};

export default withApollo({ ssr: true })(Index);
