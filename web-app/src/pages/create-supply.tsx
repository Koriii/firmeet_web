import { Heading } from '@chakra-ui/react';
import React from 'react';
import { FormattedMessage } from 'react-intl';

import { LayoutComponent } from '../common/components';
import { BodyContainer, Wrapper } from '../common/components/shared/atoms';
import { useIsAuth } from '../common/hooks/useIsAuth';
import { CreateSupplyForm } from '../models/posts/CreateSupplyForm';
import { withApollo } from '../utils/withApollo';

const CreateSupply: React.FC = () => {
  useIsAuth();

  return (
    <LayoutComponent>
      <BodyContainer type="form">
        <Wrapper variant="regular">
          <Heading as="h2" mb="4">
            <FormattedMessage id="Supply.Create.Title" defaultMessage="Create supply post" />
          </Heading>
          <CreateSupplyForm />
        </Wrapper>
      </BodyContainer>
    </LayoutComponent>
  );
};

export default withApollo({ ssr: false })(CreateSupply);
