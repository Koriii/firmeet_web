import { Box } from '@chakra-ui/react';
import React, { PropsWithChildren } from 'react';

interface IWrapperProps {
  variant?: 'small' | 'regular';
}

export const Wrapper: React.FC<PropsWithChildren<IWrapperProps>> = ({ children, variant = 'regular' }) => {
  const maxWitdth = variant === 'regular' ? '800px' : '400px';

  return (
    <Box mt={8} mx="auto" maxW={maxWitdth} w="100%">
      {children}
    </Box>
  );
};
