import { Box } from '@chakra-ui/layout';
import React from 'react';

import { FooterComponent } from './molecules';
import { HeaderComponent } from './organisms/Navigation';

const LayoutComponent: React.FC<{ children: React.ReactNode }> = ({ children }) => {
  return (
    <Box>
      <Box display="flex" flexDirection="column" justifyContent="space-between" minHeight="100vh">
        <Box>
          <HeaderComponent />
          <main id="content">{children}</main>
        </Box>
        <FooterComponent />
      </Box>
    </Box>
  );
};

export default LayoutComponent;
