import { List } from '@chakra-ui/layout';
import { ListItem } from '@chakra-ui/react';
import React from 'react';
import { IconType } from '../../../../types/enums';
import { IconListItem } from '../../atoms';

interface IIconListProps {
  listItems: string[];
  iconType?: IconType;
}

export const IconList: React.FC<IIconListProps> = ({ listItems, iconType }) => {
  return (
    <List spacing={3} textAlign="start" px={12}>
      {listItems.map((listItem, index) => (
        <ListItem key={index}>
          <IconListItem text={listItem} iconType={iconType} />
        </ListItem>
      ))}
    </List>
  );
};
