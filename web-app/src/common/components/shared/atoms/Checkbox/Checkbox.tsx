import { Checkbox, CheckboxProps } from '@chakra-ui/react';
import React from 'react';

import { colorScheme } from '../../../../../utils/constants';

interface ICheckboxProps extends CheckboxProps {}

export const CheckboxComponent: React.FC<ICheckboxProps> = ({ ...props }) => {
  return (
    <Checkbox defaultIsChecked colorScheme={colorScheme} size="size" {...props}>
      {props.children}
    </Checkbox>
  );
};
