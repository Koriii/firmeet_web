import { Box } from '@chakra-ui/react';
import { Form, Formik } from 'formik';
import { useRouter } from 'next/dist/client/router';
import React, { useCallback } from 'react';
import { useIntl } from 'react-intl';
import { LayoutComponent } from '../common/components';
import { Button, InputField, Wrapper } from '../common/components/shared/atoms';
import { MeDocument, MeQuery, useRegisterMutation } from '../generated/graphql';
import { toErrorMap } from '../utils/toErrorMap';
import { withApollo } from '../utils/withApollo';

const initRegisterValues = { email: '', password: '' };

const Register: React.FC = ({}) => {
  const router = useRouter();
  const [register] = useRegisterMutation();
  const intl = useIntl();

  const onRegisterCallback = useCallback(
    async (values, setErrors) => {
      const response = await register({
        variables: { options: values },
        update: (cache, { data }) => {
          cache.writeQuery<MeQuery>({
            query: MeDocument,
            data: {
              __typename: 'Query',
              me: data?.register.user,
            },
          });
        },
      });
      if (response.data?.register.errors) {
        setErrors(toErrorMap(response.data.register.errors));
      } else if (response.data?.register.user) {
        // worked
        router.push('/');
      }
    },
    [router, register],
  );

  return (
    <LayoutComponent>
      <Wrapper variant="small">
        <Formik
          initialValues={initRegisterValues}
          onSubmit={(values, { setErrors }) => onRegisterCallback(values, setErrors)}>
          {({ isSubmitting }) => (
            <Form>
              <Box mt={4}>
                <InputField
                  name="email"
                  type="email"
                  placeholder="john.doe@firmeet.com"
                  label={intl.formatMessage({ id: 'General.Email', defaultMessage: 'Email' })}
                />
              </Box>
              <Box mt={4}>
                <InputField
                  name="password"
                  placeholder="password"
                  label="Password"
                  type={intl.formatMessage({ id: 'General.Password', defaultMessage: 'Password' })}
                />
              </Box>
              <Button
                mt={4}
                type="submit"
                isLoading={isSubmitting}
                title={intl.formatMessage({ id: 'Registrer.RegisterButton', defaultMessage: 'Register' })}
              />
            </Form>
          )}
        </Formik>
      </Wrapper>
    </LayoutComponent>
  );
};

export default withApollo({ ssr: false })(Register);
