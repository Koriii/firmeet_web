import { GlobalAction, GlobalActionList, IGlobalState } from './models';

export const reducer = (prevState: IGlobalState, action: GlobalAction): IGlobalState => {
  switch (action.type) {
    case GlobalActionList.SET_SUPPLY_FILTER: {
      return { ...prevState, supplyFilter: { categoryId: action.payload } };
    }
    case GlobalActionList.SET_DEMAND_FILTER: {
      return { ...prevState, demandFilter: { categoryId: action.payload } };
    }
    default: {
      return { ...prevState };
    }
  }
};
