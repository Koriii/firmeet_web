import { Grid } from '@chakra-ui/react';
import React from 'react';
import { useIntl } from 'react-intl';
import { InputField, DropdownField } from '../../common/components/shared/atoms';
import { countryOptions } from '../../common/types/countries';
import { CompanyInputModel } from '../../generated/graphql';
import CompanySearchPlaceInputField from './CompanySearchPlaceInputField';

interface IAdditionalCompanyFormProps {
  setFieldValue: (field: string, value: any, shouldValidate?: boolean | undefined) => void;
  data: CompanyInputModel;
}

// TODO: Not used yet, make autocomplete work pls
export const AdditionalCompanyForm: React.FC<IAdditionalCompanyFormProps> = ({ setFieldValue, data }) => {
  const intl = useIntl();

  return (
    <Grid gridTemplateColumns={{ lg: 'repeat(3, 1fr)', md: 'repeat(2, 1fr)', sm: '1fr' }} gap={'8px'}>
      <DropdownField
        name="countryCode"
        label={intl.formatMessage({ id: 'General.CountryLabel', defaultMessage: 'Country' })}
        items={countryOptions}
      />
      <CompanySearchPlaceInputField
        componentId="district"
        placeholder={intl.formatMessage({ id: 'Address.Form.DistrictPlaceholder', defaultMessage: 'district...' })}
        label={intl.formatMessage({ id: 'Address.Form.DistrictLabel', defaultMessage: 'District' })}
        value={data}
        setFieldValue={setFieldValue}
      />
      <CompanySearchPlaceInputField
        componentId="city"
        placeholder={intl.formatMessage({ id: 'Address.Form.CityPlaceholder', defaultMessage: 'city...' })}
        label={intl.formatMessage({ id: 'Address.Form.CityLabel', defaultMessage: 'City' })}
        value={data}
        setFieldValue={setFieldValue}
      />
      <CompanySearchPlaceInputField
        componentId="postalCode"
        placeholder={intl.formatMessage({ id: 'Address.Form.PostCodePlaceholder', defaultMessage: 'postal code...' })}
        label={intl.formatMessage({ id: 'Address.Form.PostCodeLabel', defaultMessage: 'Postal code' })}
        value={data}
        setFieldValue={setFieldValue}
      />
      <CompanySearchPlaceInputField
        componentId="street"
        placeholder={intl.formatMessage({ id: 'Address.Form.StreetPlaceholder', defaultMessage: 'street...' })}
        label={intl.formatMessage({ id: 'Address.Form.StreetLabel', defaultMessage: 'Street' })}
        value={data}
        setFieldValue={setFieldValue}
      />
      <CompanySearchPlaceInputField
        componentId="streetNumber"
        placeholder={intl.formatMessage({ id: 'Address.Form.StreetNumberPlaceholder', defaultMessage: '24' })}
        label={intl.formatMessage({ id: 'Address.Form.StreetNumberLabel', defaultMessage: 'Street number' })}
        value={data}
        setFieldValue={setFieldValue}
      />
      <InputField label="Website" name="website" />
    </Grid>
  );
};
