import { Button, ButtonProps, Text } from '@chakra-ui/react';
import React, { ReactElement } from 'react';
import NextLink from 'next/link';
import { colorScheme } from '../../../../../utils/constants';

interface IButtonProps extends ButtonProps {
  size?: 'sm' | 'md' | 'lg';
  link?: string;
  href?: string;
  linkAs?: string;
  icon?: ReactElement;
  title: string;
  isForm?: boolean;
  fixedWidth?: boolean;
}

export const ButtonComponent: React.FC<IButtonProps> = ({
  size = 'sm',
  link,
  href,
  linkAs,
  icon,
  title,
  isForm = false,
  fixedWidth = false,
  ...rest
}) => {
  let button;
  if (isForm) {
    // keep for now, will change
    button = (
      <Button
        mt={4}
        fontSize={size}
        fontWeight={600}
        colorScheme={colorScheme}
        variant={rest.variant}
        href={link}
        width={fixedWidth ? '150px' : rest.width}
        {...rest}>
        {icon} <Text ml={1}>{title}</Text>
      </Button>
    );
  } else {
    button = (
      <Button
        fontSize={size}
        fontWeight={600}
        colorScheme={colorScheme}
        variant={rest.variant ?? 'solid'}
        href={link}
        width={fixedWidth ? '150px' : rest.width}
        {...rest}>
        {icon} <Text ml={1}>{title}</Text>
      </Button>
    );
  }

  return (
    <>
      {href ? (
        <NextLink href={href} as={linkAs}>
          {button}
        </NextLink>
      ) : (
        <>{button}</>
      )}
    </>
  );
};
