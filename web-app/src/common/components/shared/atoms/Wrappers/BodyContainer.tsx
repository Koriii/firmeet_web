import React from 'react';
import { Container, ContainerProps } from '@chakra-ui/react';

interface IBodyContainerProps extends ContainerProps {
  maxW?: string;
  py?: string;
  my?: string;
  type?: 'stretched' | 'bordered' | 'form';
}

export const BodyContainer: React.FC<IBodyContainerProps> = ({ maxW, py, my, type = 'bordered', ...props }) => {
  const getWidth = () => {
    if (type === 'stretched') {
      return '100%';
    }
    if (type === 'form') {
      return 'container.lg';
    }
    // type === 'bordered'
    return {
      xs: 'container.xs',
      sm: 'container.sm',
      md: 'container.md',
      lg: 'container.lg',
      xl: 'container.xl',
    };
  };

  return (
    <Container maxW={getWidth()} py={py ?? '8px'} my={my ?? '8px'} {...props}>
      {props.children}
    </Container>
  );
};
