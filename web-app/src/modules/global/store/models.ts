// idk how to do that state
export interface IGlobalState {
  demandFilter: {
    categoryId: number | null;
  };
  supplyFilter: {
    categoryId: number | null;
  };
}

export const initGlobalState: IGlobalState = {
  demandFilter: {
    categoryId: null,
  },
  supplyFilter: {
    categoryId: null,
  },
};

export interface IGlobalContextValue {
  state: IGlobalState;
  actions: IGlobalActions;
}

export enum GlobalActionList {
  SET_DEMAND_FILTER = 'SET_DEMAND_FILTER',
  SET_SUPPLY_FILTER = 'SET_SUPPLY_FILTER',
}

export type GlobalAction =
  | {
      type: GlobalActionList.SET_DEMAND_FILTER;
      payload: number;
    }
  | {
      type: GlobalActionList.SET_SUPPLY_FILTER;
      payload: number;
    };
export interface IGlobalActions {
  setFilterValue: (categoryId: number, type: any) => void;
}

export const initGlobalContextValue: IGlobalContextValue = {
  state: initGlobalState,
  actions: {
    setFilterValue: () => {
      return;
    },
  },
};
