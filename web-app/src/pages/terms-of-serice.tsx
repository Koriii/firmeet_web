import { Heading, Text } from '@chakra-ui/react';
import React from 'react';
import { LayoutComponent } from '../common/components';
import { BodyContainer } from '../common/components/shared/atoms';
import { withApollo } from '../utils/withApollo';

const TermsOfSerice = () => {
  return (
    <LayoutComponent>
      <BodyContainer>
        <Heading>GDPR</Heading>
        <Text>
          Lorem ipsum dolor, sit amet consectetur adipisicing elit. Iste expedita, rem eveniet, voluptates aperiam
          necessitatibus est suscipit, ducimus provident dicta molestiae vel totam saepe distinctio amet minima dolore
          obcaecati a? Corrupti autem quidem eligendi maxime natus obcaecati quaerat quos! Velit, voluptatem
          exercitationem? Odit beatae suscipit molestias magnam quasi, dignissimos, vel laborum minima, eveniet
          voluptates autem aliquid laboriosam. Numquam, impedit repudiandae. Id dolorum a accusantium. Facere sequi
          voluptatibus temporibus enim iste corporis ducimus repudiandae, nihil aperiam dolor quod illum necessitatibus
          quos veniam, optio quidem debitis soluta mollitia atque possimus recusandae labore. Aspernatur nostrum, sint
          exercitationem repellat magnam animi dicta voluptas nisi minus saepe voluptate eius nihil possimus suscipit
          illum quo, reprehenderit debitis. Quasi reprehenderit ducimus autem architecto est provident dolores eius.
          Laboriosam asperiores, magnam neque architecto id adipisci reprehenderit accusantium vero possimus velit ut
          dolorem labore voluptates est at iure nobis sint itaque eligendi porro illo excepturi! Obcaecati expedita
          repudiandae doloribus. Amet commodi necessitatibus ratione cupiditate provident! Laudantium illo accusantium
          vel iure culpa explicabo sed sequi adipisci at officiis quam corporis, delectus voluptatum. Possimus,
          distinctio accusantium. Voluptas sapiente quas recusandae a? Eos, sit maxime eaque beatae a sapiente magni
          nisi vitae. Perferendis facere architecto vel velit alias minus maiores, aliquam amet modi! Possimus
          voluptates id quas incidunt dolores alias neque dolorem! Repellat autem suscipit ad, numquam illo unde! Quasi
          iure ex quae! Velit consequuntur facere corrupti non illum quam nihil accusantium voluptate fugiat enim. Quo
          sunt quidem corrupti neque quos debitis! Sint error repellat optio dolore reiciendis neque, modi sed delectus,
          culpa aspernatur commodi vero expedita quia tempora laboriosam illo? Reiciendis, praesentium in reprehenderit
          vero error deserunt consectetur voluptatum illum ad? Ut harum vel expedita fugiat doloremque, nisi tempora
          iste sunt sed tenetur quod nesciunt ipsa corporis minima, at animi ex nostrum veniam atque quibusdam id dolor
          itaque maiores optio. Neque? Sapiente sequi illo porro fuga molestiae eaque nihil non incidunt cum, libero
          ipsa cumque culpa deleniti, eos expedita autem! At voluptatem, reprehenderit dolores ad commodi vitae
          assumenda error quidem soluta. Natus ratione molestias molestiae magni, consequuntur eligendi, itaque alias
          porro soluta veritatis delectus sed quod nihil iste non? Praesentium nobis harum quod rerum, labore optio amet
          odit non. Maiores, hic! Provident iste ullam libero quae veritatis aperiam dolorem, laudantium facilis illo
          dolor ipsa fugit. Perspiciatis asperiores hic magnam quisquam sequi facere consequatur libero perferendis vel
          maxime at repellendus, velit fugiat. Quos quod sapiente quas corrupti quidem nam iusto perferendis nisi ut
          fugiat beatae voluptatum, soluta accusantium laboriosam ab aut vel, eveniet esse voluptate nihil natus
          delectus provident possimus. Similique, quia? Facere quis modi eveniet necessitatibus dolore ex? Culpa sed
          ipsum quis quidem rem delectus animi perferendis! Ullam vero maiores perferendis repellendus libero aut quas
          nesciunt temporibus exercitationem, asperiores laboriosam vel! Unde, quisquam perspiciatis ducimus quibusdam
          modi exercitationem ipsum sunt voluptatum officiis tempora, quam hic, in omnis odit quis! Temporibus ullam
          itaque inventore, reiciendis saepe necessitatibus fugit debitis voluptas. Tempora, earum? Vero molestiae
          cumque, expedita consequuntur aut similique dicta dolorum accusamus soluta eveniet at quod explicabo sint
          sapiente iste? Recusandae vero aliquam sint numquam. Facilis necessitatibus hic ab veniam veritatis ipsa.
          Repudiandae officiis numquam et dolorum impedit. Soluta quas omnis est exercitationem, quidem similique
          blanditiis assumenda labore ratione voluptates impedit itaque voluptatibus magni enim earum debitis
          repellendus. Pariatur nulla esse maiores. Cupiditate quos velit perferendis aliquid possimus natus hic. Quas
          possimus similique necessitatibus! Amet impedit natus, soluta molestiae assumenda totam, provident aut
          similique atque ab doloribus obcaecati ex, quo eos officia. Repudiandae, dignissimos. Totam, dolore quasi
          culpa, ea porro at ad deserunt neque dignissimos quas non, assumenda necessitatibus praesentium officiis est
          labore minima a asperiores eius provident illo. Possimus, veniam eos. Deserunt repellat provident incidunt
          maiores? Voluptate reprehenderit ipsum doloremque tempore repellat at fugiat odio blanditiis nemo, modi neque
          earum, a sit quod praesentium corrupti! Veritatis voluptate similique tempore eos atque! Veniam magnam
          consectetur ratione facere ullam impedit quis nam eaque numquam voluptatem, consequatur nostrum voluptatibus
          amet nisi nesciunt! Ducimus deserunt sint eligendi. Quaerat dicta ea voluptatum velit minus. Repellendus,
          quibusdam? Numquam impedit incidunt tempora dolorem officiis dignissimos dolorum praesentium quaerat nemo! Nam
          dolorem rem eaque aspernatur error excepturi omnis vitae sit velit, molestias assumenda magni! Placeat unde
          quis hic quaerat. Impedit cumque provident sequi laudantium aperiam eum porro ipsam vel illo cupiditate
          pariatur voluptatibus dolore praesentium maiores vero nesciunt, facilis placeat aliquid blanditiis rerum
          dolor? Dicta doloremque facilis aut similique? Ea architecto est laudantium accusantium, exercitationem
          accusamus sunt odio, molestias modi quisquam expedita, ad id. Fugiat harum facere minus aliquam quo? Et
          perspiciatis odit, incidunt in deserunt maiores nam fugit. Corrupti perferendis amet quam voluptatem error
          excepturi voluptatum deleniti nesciunt suscipit numquam. Aspernatur, vero officiis facilis non quibusdam eum
          obcaecati deleniti, nisi odio repellat accusantium voluptates assumenda corrupti, nulla asperiores!
          Accusantium porro iste hic, officia commodi veniam fugiat facilis ut beatae velit corrupti ipsam
          necessitatibus, suscipit vel nesciunt minima nulla impedit dolor. Debitis neque assumenda vel dolor! Unde, ad
          ab! Sapiente magnam, minus temporibus earum aperiam quibusdam, numquam perferendis unde fugiat, magni maiores
          doloribus nisi. Ex dolor voluptatibus iure eveniet suscipit minima voluptatem sapiente minus. Obcaecati non
          tempora magnam. Blanditiis. Repellendus veritatis, accusantium ab cum laudantium alias? Rem blanditiis nihil,
          repudiandae unde delectus aut numquam, ratione optio excepturi quasi inventore esse perferendis vel tempore
          consectetur eaque in quam ipsam placeat! Mollitia, laboriosam odio natus, exercitationem ab quas magnam a
          temporibus minima harum repellendus sequi et dolorem officiis doloremque facere vel blanditiis fugit pariatur
          distinctio provident animi. Labore commodi cupiditate consequuntur? Harum eos, saepe excepturi nemo nulla
          voluptates rem quos in neque temporibus expedita maxime earum pariatur optio vel quasi at voluptate, itaque
          sequi reprehenderit perspiciatis, corrupti alias facere. Quibusdam, dicta? Voluptatem totam illum sequi quod
          explicabo adipisci, nisi pariatur quo minus tempora quisquam velit! Voluptatem aut a suscipit sit molestias
          exercitationem quo recusandae natus dolores culpa, enim, voluptatibus cum nihil. Saepe aspernatur veritatis
          dolore nobis suscipit, incidunt eligendi? Id accusamus delectus fuga ducimus ex iure iusto mollitia distinctio
          assumenda odit nostrum ratione dolore, consequuntur at ut vero sunt, sapiente dicta! Harum porro maiores odit
          velit labore aliquid ipsa aut fuga officia, nostrum beatae. Ducimus iusto commodi, ipsum saepe molestias fuga
          quod sequi sint debitis necessitatibus nemo adipisci? Ad, doloremque sed. Vel suscipit facilis soluta. Ipsam
          modi quod sit ex assumenda porro nemo nobis rem labore numquam dolorum placeat fuga neque recusandae ut
          inventore accusantium sint, cum sequi ullam consectetur necessitatibus! Sequi eius voluptates error aliquam
          velit, voluptatem magni nulla asperiores laborum mollitia quasi, ipsum molestias eum, porro hic illo fuga.
          Aspernatur veniam officia, eius doloribus sunt perferendis animi qui atque! Impedit odio explicabo sed amet
          omnis magnam molestias, illo, molestiae quam tempora voluptates? Vel animi officia excepturi commodi
          distinctio, delectus enim vero. Sed qui a excepturi temporibus voluptatem perspiciatis dolorem. A magni minima
          voluptatem unde voluptate eos quaerat, eum eveniet harum incidunt, adipisci, ab perspiciatis fuga accusamus
          nostrum ducimus hic. Necessitatibus perspiciatis autem non modi iusto saepe cumque. Deleniti, facere. Natus
          ullam pariatur cum quaerat ducimus minima vero quisquam perferendis molestias corrupti quia voluptas vitae,
          deleniti facere similique ut ab minus iure tempore debitis iusto molestiae! Impedit, unde? Exercitationem,
          distinctio! Neque iste, cupiditate ipsam nemo quis eius libero quam consectetur dignissimos explicabo nesciunt
          doloribus, repellat odio aperiam et atque perspiciatis aliquid nulla laborum fugiat, repudiandae
          reprehenderit. Enim sed qui consequatur! Harum libero odio accusamus soluta non quidem tempora pariatur
          mollitia magnam aut dignissimos molestiae, nihil aperiam velit facere ipsum rerum quod, explicabo a voluptas
          voluptatibus voluptates? Nostrum velit qui rem. Doloribus corporis sit tenetur perferendis quas repudiandae
          ullam libero saepe iste, earum excepturi magnam quidem non culpa, fugit sequi laborum vel voluptatibus
          nesciunt. Quam cum neque aliquid perferendis. Tenetur, libero. Consequuntur quam consequatur perferendis atque
          est dolore commodi eius voluptatibus nulla nemo laudantium eveniet pariatur, tempora modi voluptates quo.
          Aliquam quam corrupti exercitationem quo quasi debitis animi expedita ut ullam? Obcaecati fuga magni
          repudiandae natus iste architecto porro molestiae cum ut hic dolorum, impedit facilis cumque, atque, nisi
          culpa omnis illum? Tempora error facilis nihil nisi sed ab, iusto inventore. Rem excepturi deserunt
          praesentium sint adipisci quia nobis aspernatur ad veniam repudiandae eaque illum, perspiciatis, ducimus
          repellat explicabo quisquam pariatur molestias. Libero odio accusamus quia minus quisquam obcaecati modi
          perspiciatis? Sunt, praesentium a. A provident quos consequuntur nam dolorum non porro asperiores quam
          exercitationem, magnam commodi molestias rerum ipsum sint, saepe eos consequatur fugit unde doloribus neque,
          rem dolor? Provident? Eum aut minima iusto impedit molestiae! Tempora omnis ad iusto corporis, sed error, aut
          eius perspiciatis soluta quo laborum? Perspiciatis quisquam et tenetur nesciunt hic earum officia incidunt
          labore ad. Itaque aut harum est odio. Eveniet cupiditate, quis nostrum quidem nisi quibusdam explicabo
          aliquam, consectetur impedit ipsa accusantium eaque inventore fugiat ut, rerum perspiciatis. In officiis
          eveniet praesentium aperiam maxime! Magnam hic fugiat commodi aliquid tempora ratione sequi molestiae ducimus
          explicabo repudiandae, maiores, recusandae nam quia consequuntur provident asperiores ipsum eveniet magni iste
          rem molestias. Placeat odio minima obcaecati cum! Totam adipisci, eligendi eaque perferendis temporibus
          officia hic eius soluta illum, praesentium corrupti repellat tempora! Quasi placeat sint iste, unde maiores id
          fuga, doloribus quas voluptate expedita ipsam adipisci pariatur. Nulla veniam adipisci cupiditate, repellat
          dolor at fugit in quam ex consequatur cumque quaerat laborum eaque, tempore expedita magni vel, non debitis
          architecto nihil itaque enim voluptate? Beatae, perferendis sint. Dolores deleniti architecto eaque natus quod
          dicta aperiam quis totam, unde animi! Veritatis vel amet debitis quos nobis, eum in vitae deleniti, similique,
          ex iusto sit tenetur tempora maiores ipsum? Optio accusamus suscipit provident possimus earum dicta tenetur
          pariatur totam voluptates, atque illum nam reiciendis sequi maiores a similique laudantium fugiat mollitia
          adipisci obcaecati voluptatem id quam incidunt? A, repellendus! Odit dignissimos nisi perspiciatis provident
          quibusdam eaque repellendus fugiat dolores reprehenderit neque tenetur ratione voluptatum, vitae corporis
          dolore placeat distinctio omnis a aliquid, culpa perferendis tempora rem optio odio. Rem! Ut inventore
          aspernatur pariatur quasi dolores nam, ipsam error non facilis sequi, ea dolorum possimus eligendi repudiandae
          dicta enim minus qui. Reiciendis alias porro mollitia quibusdam doloribus ut commodi eligendi? Vel sapiente
          modi molestias libero a ipsam soluta voluptatibus culpa quidem nihil corrupti quaerat et minima incidunt saepe
          aspernatur, dignissimos corporis assumenda commodi numquam consequuntur quibusdam harum. In, molestiae error?
          Dignissimos provident iure voluptates non sequi perferendis natus ullam esse eius, illo modi beatae odio atque
          iusto voluptatum debitis est suscipit laborum earum voluptas? Tenetur nulla dicta corporis ratione voluptas!
          Esse, tempore similique, autem qui doloremque, quaerat consectetur numquam accusamus vel minus fugiat. Commodi
          ipsum voluptate iusto cupiditate quae sunt, id doloremque natus eveniet illo ipsam quia nostrum blanditiis
          perspiciatis. Impedit quaerat ratione voluptatem minima temporibus reiciendis eius fugiat ullam, voluptate,
          consequuntur distinctio recusandae qui dicta molestias nisi dolor debitis repellendus veniam accusamus atque
          dolorem id neque officia pariatur! Vitae. Fugit veniam reprehenderit pariatur qui fuga aperiam eligendi atque,
          expedita sit, nihil eveniet incidunt labore natus corrupti maxime cum enim? Accusamus necessitatibus, nesciunt
          in consequatur incidunt id dicta nemo eaque? Aspernatur eligendi dolor dolorum nulla molestias amet dolorem,
          ducimus dignissimos animi, tempore sunt rerum exercitationem dolore id facilis iusto cumque distinctio
          consequuntur voluptas qui aperiam illum a quo ipsam. Facere. Reprehenderit ut quia iste voluptas quidem nisi,
          itaque dolorum soluta ab deleniti, suscipit consequatur. At distinctio quasi tempore, doloribus inventore
          ducimus maiores asperiores quisquam ut numquam, et animi laborum dolores! Est cumque eum quae, minus beatae
          eligendi. Soluta hic ut sapiente. Nemo veritatis, iure, ipsum autem natus distinctio quibusdam ipsa suscipit
          quas cumque molestias optio, libero reiciendis incidunt porro facere. Quia minima at, eum reprehenderit
          eligendi in iure possimus aliquam voluptate nobis explicabo repellendus! Pariatur, deleniti voluptates non
          natus illo sed doloribus ex et ducimus cupiditate, rerum expedita, numquam ratione. Sed mollitia hic a, cumque
          reiciendis, dicta sapiente soluta rerum labore assumenda molestias enim facere minus animi illo quam non
          tempore sit exercitationem nam aliquid vero saepe culpa laborum? In. Inventore fugit dicta suscipit ut officia
          voluptates. Vel quidem assumenda rem, ipsum similique iure placeat, deleniti amet reprehenderit maxime totam
          nam accusamus doloribus aut, repellat ullam delectus impedit sed blanditiis. Aspernatur molestias nihil
          eveniet! Consectetur aperiam, iste, eos rem aliquam magnam voluptatibus expedita totam saepe non sapiente
          amet, dicta molestiae. Quo repellendus voluptatum vero blanditiis libero. Modi totam accusamus asperiores?
          Ducimus consequuntur praesentium inventore nisi unde fugiat omnis quasi accusamus eaque commodi dolorem ullam
          ipsam, ea perspiciatis reprehenderit. Eligendi quam autem soluta. Ipsa tempora aut nesciunt placeat a ea
          blanditiis. Perspiciatis vel iusto unde explicabo a aperiam perferendis minus optio recusandae placeat!
          Quisquam ab, reiciendis inventore eligendi corrupti, quae optio itaque rerum distinctio eius officiis error
          nesciunt nostrum vitae aspernatur. Quo sed voluptas suscipit qui incidunt quos quas necessitatibus quod, rem
          dolor nobis pariatur ipsa aut est blanditiis fuga accusantium corporis sunt eligendi quisquam! Labore
          inventore reprehenderit blanditiis rem. Nulla? Omnis magni nisi error exercitationem voluptates, saepe alias
          quis cum at et, vitae rem, assumenda nobis vel! Labore officiis repellat, repellendus et tempora dignissimos
          neque harum, quos vitae nostrum similique? Minima quidem neque, nisi sunt facere laboriosam id cum esse?
          Cumque libero illum cum omnis quisquam sunt perspiciatis accusamus, repudiandae numquam exercitationem!
          Quaerat, eos quae commodi corrupti minus accusantium veritatis. Ab temporibus veniam soluta adipisci? In ad
          nobis amet incidunt nihil eaque corrupti eius earum odio tempore veniam cumque ut nam temporibus inventore
          quibusdam officia alias, labore nemo deleniti mollitia. Pariatur qui odio quam, dolore nam, provident deserunt
          aliquid esse voluptas, similique harum quidem! Commodi, deserunt exercitationem necessitatibus fuga assumenda
          velit minus maiores aliquid, cumque distinctio ratione qui, laboriosam eius! Error eveniet, repudiandae cumque
          atque voluptatum deleniti dolorem? Assumenda doloremque itaque aspernatur sunt libero harum dolor, expedita id
          nostrum illo veritatis, perferendis molestias voluptas alias distinctio, deleniti corrupti cumque dignissimos?
          Voluptatum, id explicabo. Voluptatum, vitae modi itaque quis aliquam mollitia distinctio repudiandae quibusdam
          laudantium ducimus molestiae fuga ad eum ex cum natus dolores quidem aliquid qui libero quisquam nesciunt
          minima! Nesciunt rem, commodi qui deserunt iusto officiis numquam explicabo asperiores beatae libero sint
          harum laboriosam voluptate ratione, incidunt ullam nulla eos non. Reiciendis dignissimos maxime quos minima,
          eum recusandae iure. Minus doloribus reiciendis perspiciatis dolor, voluptates eos adipisci ex modi ullam
          vitae dolorum in expedita veniam a explicabo dolore qui ratione. Ad esse neque nobis possimus, porro
          necessitatibus minima. Nam! Possimus itaque pariatur quam id ut eos nemo laboriosam dolores deserunt, qui
          tempora, recusandae error quisquam aperiam. Nobis soluta commodi dolore dolores unde ad impedit eligendi,
          aliquid blanditiis sint ullam? Labore minus cum eaque quasi quam nisi vitae quod dolores inventore culpa fugit
          consequatur porro dignissimos, accusantium tenetur aliquid incidunt adipisci laboriosam voluptatum ex! Itaque,
          iste aliquam. Illum, sunt ratione. Nulla quae harum neque eos odio id temporibus veniam expedita. Officiis
          labore quae, cum soluta fuga ipsa aperiam, modi voluptatibus excepturi voluptate atque odit eum animi delectus
          ab illum ea! Recusandae ad aut voluptates laboriosam, exercitationem ipsum nisi accusamus accusantium alias
          incidunt, commodi explicabo odio repudiandae dolore ab vel velit quas porro. Ex, eos at sed unde voluptatum
          temporibus tempore? Voluptatum, sit. Optio dolores nulla quibusdam similique, neque facilis doloribus
          consequatur voluptatem nostrum delectus repellendus, enim voluptates ut blanditiis modi culpa sed error hic
          consectetur! Repellat harum impedit mollitia corrupti. Consectetur nisi modi sequi earum ad, nesciunt quisquam
          cumque voluptas quis, fugiat velit laudantium labore recusandae distinctio nihil doloribus praesentium minima
          veritatis saepe tenetur aliquid molestiae. A nemo veniam illo! Nostrum, qui illo quis ut magni eos iusto at,
          deserunt aperiam amet corporis similique reprehenderit ipsa autem totam quod. Eveniet autem quaerat voluptas
          perspiciatis sunt, vel illo tempore tempora voluptates. Dolorum, consequuntur placeat consequatur asperiores
          dolorem sit ratione unde enim quae necessitatibus esse dolor nam ea tempora qui quas repudiandae incidunt
          maxime sint dolore. Neque amet quas deleniti beatae porro. Impedit cumque in laudantium corrupti recusandae
          aspernatur eaque eligendi id, corporis est autem, ullam explicabo consequatur beatae error consequuntur harum
          praesentium? Non laborum suscipit explicabo pariatur ad beatae aspernatur autem. Cupiditate soluta ipsa
          doloremque accusantium ab. Explicabo blanditiis itaque dolores facilis necessitatibus culpa illum beatae,
          deserunt reprehenderit maxime mollitia sapiente praesentium corrupti animi vitae velit laudantium ullam saepe
          ipsum voluptas? Quos, cupiditate dignissimos. Quo corporis et delectus unde quas commodi illo alias
          repellendus facilis sunt neque temporibus rem, consequuntur architecto omnis dolores accusantium facere est
          sit nobis, distinctio, earum harum? Aliquam deleniti doloribus at, ea est, qui ab reprehenderit blanditiis
          modi explicabo tenetur obcaecati suscipit sit sunt atque aperiam. Dolorum tempore assumenda facere alias
          dolores nihil molestias ipsam! Cumque, possimus. Labore cumque assumenda deserunt, sit minima fugiat ratione
          corporis non ipsum porro quod aliquid eveniet saepe distinctio unde. Temporibus repellendus quas consectetur
          voluptatibus facere maiores modi fugiat maxime deleniti id. Animi, quaerat laboriosam id sint aut molestias
          beatae, illum tempora consequatur quidem odio sit totam aliquam aspernatur qui velit error quod officiis
          voluptatum magnam, magni dolor quae. Dolores, facilis corrupti! Tempora enim at saepe eum atque esse? Suscipit
          dicta molestias dignissimos nihil odit quam mollitia exercitationem debitis, fuga minima porro ullam
          doloremque, pariatur odio deleniti quae omnis labore cupiditate itaque. Harum, sequi architecto nihil maxime
          aliquid quia ipsa cum repellat unde consectetur numquam voluptatibus omnis ex accusamus quam excepturi odit
          sunt iusto esse eligendi? Accusamus repellat quas qui porro magni. Facere id nesciunt fuga voluptas quisquam
          unde ullam blanditiis et. Asperiores doloremque animi quae veniam debitis! Voluptatibus fuga quaerat, soluta
          labore neque ipsum dolores praesentium quod voluptate quos fugit sint. Doloribus cumque sit recusandae itaque?
          Eius, porro. Obcaecati, et reiciendis praesentium nemo distinctio ex fugit tempore, aut, maiores quisquam
          omnis repellat animi molestias ratione eius modi voluptatum quod magnam! Assumenda. Non, sapiente perferendis!
          Esse voluptas doloremque illo dicta quo. Ad illum perspiciatis voluptatem ratione eligendi neque, quo totam,
          cum provident est expedita quas autem in vitae eum, dolorem similique iusto? Explicabo, quia necessitatibus
          facere mollitia ad alias sapiente exercitationem corporis omnis autem culpa veritatis maiores? Sit iure fugit
          laudantium fugiat, reprehenderit, pariatur blanditiis aliquid vel at, temporibus eum voluptates incidunt.
          Perferendis accusamus molestias nam dolorum aspernatur est ducimus iure architecto dicta, quidem velit
          explicabo consequatur praesentium deleniti in distinctio minus, totam assumenda unde dolor ipsa temporibus
          mollitia a culpa! Excepturi! Accusantium corrupti voluptate maxime repellendus iste optio veritatis,
          blanditiis fuga ipsa modi quod iusto voluptates maiores provident quo, nam enim nobis expedita sit. Ut totam
          deserunt, minus neque unde nostrum.
        </Text>
      </BodyContainer>
    </LayoutComponent>
  );
};

export default withApollo({ ssr: true })(TermsOfSerice);
