import { Heading, Text } from '@chakra-ui/layout';
// import { Flex, Spinner, Stack } from '@chakra-ui/react';
// import { faSave, faTrash } from '@fortawesome/free-solid-svg-icons';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { Form, Formik } from 'formik';
// import { useRouter } from 'next/router';
// import React, { useCallback, useMemo } from 'react';

// import { LayoutComponent } from '../../../../common/components';
// import { BodyContainer, Button, Dropdown, InputField } from '../../../../common/components/shared/atoms';
// import { useGetDemandPostById } from '../../../../common/hooks/useGetPostFromUrl';
// import {
//   useCompanyOptionsQuery,
//   useDeleteDemandPostMutation,
//   useMeQuery,
//   useUpdateDemandPostMutation,
// } from '../../../../generated/graphql';
// import { isServerSide } from '../../../../utils/isServer';
// import { withApollo } from '../../../../utils/withApollo';

// interface IUpdateFormValues {
//   title: string;
//   text: string;
//   companyId: number;
// }

// const DemandEditPost: React.FC = ({}) => {
//   const router = useRouter();
//   const formattedId = useMemo<number>(() => Number(router.query.id), [router.query.id]);

//   const { data, loading } = useGetDemandPostById(formattedId);
//   const { data: meData } = useMeQuery({
//     skip: isServerSide(),
//   });
//   const { data: companyOptions } = useCompanyOptionsQuery({ variables: { creatorId: meData?.me?.id ?? 0 } });

//   const [deletePost] = useDeleteDemandPostMutation();
//   const [updatePost] = useUpdateDemandPostMutation();

//   const onDeletePostCallback = useCallback(
//     async (id?: number) => {
//       if (id) {
//         await deletePost({
//           variables: { id },
//           update: (cache) => {
//             cache.evict({ id: 'Demand:' + id });
//           },
//         });
//         router.push('/demand-list');
//       }
//     },
//     [deletePost, router],
//   );

//   // no need to update cache because URQL updates the post object with new values from UpdatePost.graphql
//   const onSubmitPostCallback = useCallback(
//     async (values: IUpdateFormValues) => {
//       await updatePost({ variables: { id: formattedId, input: values } });
//       // router.push(`/demand-list/post/${formattedId}`);
//       router.back();
//     },
//     [updatePost, router],
//   );

//   let content;
//   if (loading) {
//     content = (
//       <BodyContainer textAlign="center">
//         <Spinner size="xl" />
//       </BodyContainer>
//     );
//   } else if (!data?.demandPost) {
//     content = (
//       <BodyContainer textAlign="center">
//         <Heading mb="4">404: Not Found</Heading>
//         <Text>You just hit a route that doesn&#39;t exist... the sadness.</Text>
//       </BodyContainer>
//     );
//   } else {
//     content = (
//       <BodyContainer>
//         <Formik
//           initialValues={{
//             title: data.demandPost.title,
//             text: data.demandPost.text,
//             companyId: data.demandPost.companyId,
//           }}
//           onSubmit={(values) => onSubmitPostCallback(values)}>
//           <Form>
//             <InputField label="Title" name="title" />
//             <InputField label="Body" name="text" rows={16} textarea />
//             <Dropdown name="companyId" label="Company" items={companyOptions} />

//             <Flex>
//               <Stack direction="row" spacing={4} ml="auto" mt="4">
//                 <Button type="submit" icon={<FontAwesomeIcon icon={faSave} width={10} height={10} />} title="Save" />
//                 <Button
//                   colorScheme="red"
//                   onClick={() => onDeletePostCallback(data.demandPost?.id)}
//                   icon={<FontAwesomeIcon icon={faTrash} width={10} height={10} />}
//                   title="Delete"
//                 />
//               </Stack>
//             </Flex>
//           </Form>
//         </Formik>
//       </BodyContainer>
//     );
//   }

//   return <LayoutComponent>{content}</LayoutComponent>;
// };

// export default withApollo({ ssr: false })(DemandEditPost);
