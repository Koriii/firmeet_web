import { Stack } from '@chakra-ui/react';
import React from 'react';
import { NAV_ITEMS } from '../constants';
import { MobileNavItem } from './MobileNavItem';
import { useColorModeValue } from '@chakra-ui/color-mode';

export const MobileNav: React.FC = () => {
  return (
    <Stack bg={useColorModeValue('white', 'gray.800')} p={4} display={{ md: 'none' }}>
      {NAV_ITEMS.map((navItem) => (
        <MobileNavItem key={navItem.label} {...navItem} />
      ))}
    </Stack>
  );
};
