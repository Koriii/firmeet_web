import { UserFieldError } from '../generated/graphql';

export const toErrorMap = (errors: UserFieldError[]) => {
  const errorMap: Record<string, string> = {};
  errors.forEach(({ field, message }) => {
    errorMap[field] = message;
  });

  return errorMap;
};
