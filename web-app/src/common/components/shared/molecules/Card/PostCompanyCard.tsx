import { Box, Grid, Text } from '@chakra-ui/react';
import React from 'react';

import { Maybe } from '../../../../../generated/graphql';
import { CompanyBorderBox } from '../../atoms';

interface ICompanyPostDetail {
  name: string;
  cin?: Maybe<string>;
  vat?: Maybe<string>;
  street: string;
  city: string;
  website?: Maybe<string>;
  district: string;
  postalCode: string;
  streetNumber: string;
  countryCode: string;
}

interface ICompanyView {
  data: ICompanyPostDetail;
  isBlured?: boolean;
}

const bluredObject = { color: 'transparent', textShadow: '0 0 8px rgba(0,0,0,0.8)' };

export const PostCompanyCard: React.FC<ICompanyView> = ({ data, isBlured = false }) => {
  const blured = isBlured ? bluredObject : {};

  return (
    <CompanyBorderBox>
      <Grid templateColumns={['repeat(2, 1fr)']} gap={6}>
        <Box>
          <Text size="lg" fontWeight={700} {...blured}>
            {data.name}
          </Text>
          <Text {...blured}>CIN: {data.cin}</Text>
          {data.vat && <Text {...blured}>VAT: {data.vat}</Text>}
          <Text {...blured}>{data.website}</Text>
        </Box>
        <Box>
          <Text {...blured}>
            {data.streetNumber} {data.street}
          </Text>
          <Text {...blured}>
            {data.city}, {data.countryCode}
          </Text>
          <Text {...blured}>{data.postalCode}</Text>
        </Box>
      </Grid>
    </CompanyBorderBox>
  );
};
